海创内部SDK

集成常用工具类

### 修订记录




| 版本  |    时间    | 修订人 | 说明 |
| ----- | :--------: | ------ | ------ |
| 1.1.8 | 2022-02-17 | hqs | 图片信息增加字段（修改时间、路径） |
| 1.1.7 | 2021-11-14 | hqs | ApkUtil增加异常捕获、NetWorkUtil增加说明、PackageUtil增加异常捕获、实体类保持不被混淆 |
| 1.1.6 | 2021-11-02 | hqs | fix PhoneContactsUtil getAllContacts 出现的Cursor window could not be created from binder |
| 1.1.5 | 2021-11-01 | hqs | 1、fix手机卡数量获取null处理 |
|  |  |  | 2、调整设备信息字段顺序 |
|  |  |  | 3、完善DeviceUtil 设备信息字段【phoneModel】获取方式 |
|  |  |  | 4、完善DeviceUtil 设备信息字段【getAndroidId】出现的9774d56d682e549c值处理 |
|  |  |  | 5、完善DeviceUtil Context null兼容处理 |
|  |  |  | 6、完善DeviceUtil notHasLightSensorManager 模拟器判断 |
|  |  |  | 7、完善DeviceUtil isRooted 方法更多情况 |
| | | | 8、清单文件去除权限【READ_PRIVILEGED_PHONE_STATE】 |
| | | | 9、新增RomUtil 、ShellUtil工具类 |
| 1.1.4 | 2021-10-28 | liwei | 1、设备信息增加手机卡的数量、设备运营商名称 |
| 1.1.3 | 2021-10-22 | hqs | 1、修复抽取选择联系人工具 |
| 1.1.2(勿用) | 2021-10-22 | hqs | 1、修复抽取选择联系人工具 |
| 1.1.1(勿用) | 2021-10-22 | hqs | 1、抽取选择联系人工具 |
| 1.1.0 | 2021-10-20 | hqs | 1、发布正式版1.1.0，stepview 支持空字符串不占位显示 |
| 1.0.1-rc9 | 2021-10-15 | hqs | 1、完善AppListParam返回null处理 |
|  |  |  | 2、完善PhoneContact返回null处理 |
|  |  |  | 3、完善SmsDataBean返回null处理 |
| 1.0.0-rc8 | 2021-09-29 | hqs | 修复图片工具类抓取异常 |
| 1.0.0-rc7 | 2021-09-28 | hqs | 1、fix 网络异常捕捉 |
|  |  |  | 2、设备信息增加字段  |
|  |  |  | 2、【设备当前的系统开发代号，一般使用REL代替】【codeName】  |
|  |  |  | 2、【获取设备指令集名称】【cpu】  |
|  |  |  | 2、【cpu 的信息】【cpuInfo】  |
|  |  |  | 2、【设备的软件版本号：返回移动终端的软件版本】【deviceSoftwareVersion】  |
|  |  |  | 2、【设备硬件名称,一般和基板名称一样】【hardware】  |
|  |  |  | 2、【语言】【language】  |
|  |  |  | 2、【运营商】【networkOperator】  |
|  |  |  | 2、【运营商名称】【networkOperatorName】  |
|  |  |  | 2、【整个产品的名称】【product】  |
|  |  |  | 2、【无线电固件版本号】【radioVersion】  |
| 1.0.0-rc6 | 2021-09-25 | hqs | 完善图片信息抓取工具类 |
| 1.0.0-rc5 | 2021-09-25 | hqs | 图片信息增加字段photoId |
| 1.0.0-rc4 | 2021-09-23 | liwei | 图片信息抓取工具类 |
| 1.0.0-rc3 | 2021-09-23 | hqs | 短信抓取工具类 |
| 1.0.0.-rc2 | 2021-09-15 | hqs | 1、新增复制 |
|  |  |  | 2、方法新增权限说明 |
| 1.0.0-rc1 | 2021-09-10 | hqs    | 第一版 |
|       |            |        |        |





### 仓库地址

https://gitee.com/yz1309/HicloudSdks



### 使用

1. Add it in your root build.gradle at the end of repositories:

   ```
   allprojects {
   		repositories {
           ...
   			maven { url 'https://jitpack.io' }
   		}
   }
   ```

   

2. Add the dependency

   ```
   dependencies {
   	    implementation 'com.gitee.yz1309:HicloudSdks:1.0.0-rc8'
   }
   ```

   

### 包名
com.hc.hicloudsdk



### 项目结构

+ src/main/java/...
  + bean
    + AppListParam
    + PhoneContact
    + PicDataParam
    + SignParam
    + SmsDataBean
  + ext
    + Standard
  + utils
    + AlbumUtil
    + ApkUtil
    + DateUtil
    + DeviceUtil
    + FileUtil
    + NetWorkUtil
    + PackageUtil
    + PhoneContactsUtil
    + PhoneUtil
    + RegexUtil
    + SizeUtil
    + ZipUtil
  + RegexUtil
    + 
  + view
    + decor
      + DefaultDecoration
      + DividerOrientation
      + SpaceItemDecoration



#### kotlin拓展类：Standard

kotlin拓展类

```
模拟三目运算符操作
tvTitle.visibility = ternaryOperator(isChecked,View.INVISIBLE,View.VISIBLE)

......
```



#### 工具类：AlbumUtil

图片信息抓取工具类

```
@Notice： 调用方法前，请保证已获取{AlbumUtil.permissions}里所包含权限
```

```
 获取设备图片信息｛List<PicDataParam>｝
 AlbumUtil.getPicDatas(activity,timestamp)
```



#### 工具类：ApkUtil

Apk工具类

```
 获取手机已安装的App｛AppListParam｝
 ApkUtil.scanLocalInstallAppList(PackageManager)
 
 判断应用程序是否是用户程序｛Boolean｝ 
 ApkUtil.filterApp(ApplicationInfo)
 
 判断指定应用是否安装 ｛Boolean｝ 
 ApkUtil.checkSpecifiedAppInstalled(this,packageName)
 
 安装一个apk ｛void｝ 
 ApkUtil.installApk(this,"",file)
 
 打开对应应用市场跳转到指定App ｛void｝ 
 ApkUtil.openSpecifiedStoreUpdate(this,appPkg)
 
 打开GooglePlayStore跳转到指定App ｛void｝ 
 ApkUtil.openGooglePlayStoreJumpToSpecifiedApp(this,packageName)
 
 获取手机已安装的App｛AppListParam｝ApkUtil.scanLocalInstallAppList(PackageManager)
 @Notice： 调用方法前，请保证保证已配置权限｛android.permission.QUERY_ALL_PACKAGES｝
 
 判断应用程序是否是用户程序｛Boolean｝ ApkUtil.filterApp(ApplicationInfo)
 
 判断指定应用是否安装 ｛Boolean｝ ApkUtil.checkSpecifiedAppInstalled(this,packageName)
 
 安装一个apk ｛void｝ ApkUtil.installApk(this,"",file)
 @Notice： 调用方法前，请保证已获取权限｛Manifest.permission.REQUEST_INSTALL_PACKAGES｝
 
 打开对应应用市场跳转到指定App ｛void｝ ApkUtil.openSpecifiedStoreUpdate(this,appPkg)
 
 打开GooglePlayStore跳转到指定App ｛void｝ ApkUtil.openGooglePlayStoreJumpToSpecifiedApp(this,packageName)
```



#### 工具类：DateUtil

时间格式



#### 工具类：DeviceUtil

设备信息工具类

```
@Notice： 调用方法前，请保证已获取权限｛Manifest.permission.READ_PHONE_STATE｝
```

```
获取完整的设备信息｛SignParam｝
DeviceUtil.getDeviceInfo(context,afAndroidId,googleAdId)

获取UUID ｛String｝
DeviceUtil.uuid

......
```



#### 工具类：FileUtil

文件工具类

```
@Notice： 调用方法前，请保证已获取权限｛Manifest.permission.READ_EXTERNAL_STORAGE、Manifest.permission.WRITE_EXTERNAL_STORAGE｝
```

```
获取缓存大小｛String｝
FileUtil.getTotalCacheSize(context)

清除缓存｛｝
FileUtil.clearAllCache(context)

创建Camera File对象｛File｝
FileUtil.getCameraFile(context,"xx.png")

......
```



#### 工具类：NetWorkUtil

网络工具类

```
@Notice： 调用方法前，请保证已获取权限｛Manifest.permission.INTERNET、Manifest.permission.ACCESS_NETWORK_STATE、Manifest.permission.ACCESS_WIFI_STATE｝
```

```
 判断已连接的WIFI是否可用｛Boolean｝
 NetWorkUtil.isWiFiAvailable(context)
 
 获取当前网络连接类型｛String｝
 NetWorkUtil.getNetworkState(context)
 
 获取wifi mac地址｛String｝
 NetWorkUtil.getWifiMac(context)
 
 获取wifi name ｛String｝
 NetWorkUtil.getWifiName(context)
```



#### 工具类：PackageUtil

安装包工具类

```
 获取应用程序的包名｛String｝ 
 PackageUtil.getPackageName(this)
 
 获取应用程序的版本号 ｛Long｝ 
 PackageUtil.getVersionCode(this)
 
 获取应用程序的外部版本号 String｝
 PackageUtil.getVersionName(this)
 
 读取manifest.xml中application标签下的配置项，如果不存在，则返回空字符串 ｛String｝ 
 PackageUtil.getConfigString(this,"appId")
 
 读取manifest.xml中application标签下的配置项 ｛Int｝ 
 PackageUtil.getConfigInt(this,"app")
 
 读取manifest.xml中application标签下的配置项 ｛Boolean｝ 
 PackageUtil.getConfigBoolean(this,"channel")
```



#### 工具类：PhoneContactsUtil

获取联系人工具类

```
@Notice： 调用方法前，请保证已获取权限｛Manifest.permission.READ_CONTACTS｝
```

```
获取手机所有联系人{ArrayList<PhoneContact>}：
PhoneContactsUtil.getAllContacts(this)

获取手机所有联系人{String[]}：PhoneContactsUtil.getAllContacts(this,PhoneContactsUtil.contentUri)
```



#### 工具类：PhoneUtil

手机号码工具类

```
@Notice： 调用方法前，请保证已获取权限｛Manifest.permission.CALL_PHONE｝
```

```
拨打电话：PhoneUtil.call(this,"17780461309")

```



#### 工具类：QRUtil

QR二维码工具类base@zxing

```
生成自定义二维码｛Bitmap｝
QRUtil.createQRCodeBitmap(content,800,800,"UTF-8","H","1",Color.BLACK,Color.WHITE,null,0f,null));

......
```



#### 工具类：RegexUtil

正则匹配工具类

```
是否为 匹配正则{Boolean}：
RegexUtil.isMatch("17780461309","^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\d{8}$")
```



#### 工具类：SizeUtil

dp、px 工具转换类

```
dp 转 px｛Int｝
SizeUtils.dp2px(context,2)

sp 转 px｛Int｝
SizeUtils.sp2px(context,2)

px 转 sp｛Int｝
SizeUtils.px2sp(context,2)
```



#### 工具类：ZipUtil

压缩工具类

```
将ByteArray 压缩成字符串｛SignParam｝
ZipUtil.decompressToStringForZlib(ByteArray)

......
```



#### View类：DefaultDecoration

用于RecyclerView分割线



#### View类：SpaceItemDecoration

用于RecyclerView分割线



#### View类：StepView

步骤View

```
 <com.hc.hicloudsdk.view.StepView
          android:id="@+id/step_view_inc"
          android:layout_width="match_parent"
          android:layout_height="wrap_content"
          app:svSelectedColor="@color/app_theme_color" />
          
 val steps = Arrays.asList(*arrayOf("第一步"), "第二步", "第三步"))
 stepViewInc.setSteps(steps)
```



#### View类：BottomNavigationViewEx

用于首页底部导航

```
<com.hc.hicloudsdk.view.bottomnavigation.BottomNavigationViewEx
        android:id="@+id/mainBottom"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="#f8f8f8"
        android:theme="@style/myNavigationDrawerStyle"
        app:itemIconSize="30dp"
        app:itemIconTint="@color/bottom_selector"
        app:itemTextColor="@color/bottom_selector"
        app:labelVisibilityMode="labeled"
        app:menu="@menu/menu_main" />
        
        mainBottom.init {
                when (it) {
                    R.id.tab_home -> {
                        mainViewpager.setCurrentItem(0, false)
                    }
                }
            }
        mainBottom.interceptLongClick(R.id.tab_home)
```

