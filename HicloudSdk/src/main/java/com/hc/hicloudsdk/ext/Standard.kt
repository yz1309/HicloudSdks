package com.hc.hicloudsdk.ext

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import com.hc.hicloudsdk.R

/**
 * 当且仅当当前 [CharSequence] 不为 `null` 或 空串时才执行 [block]。
 */
fun <T : CharSequence> T?.applyIfNotEmpty(block: (T) -> Unit) {
    if (this?.isNotEmpty() == true) {
        block.invoke(this)
    }
}

/**
 * 当且仅当当前 [CharSequence] 为 `null` 或 空串时才执行 [block]。
 */
fun <T : CharSequence> T?.applyIfEmpty(block: () -> Unit) {
    if (this.isNullOrEmpty()) {
        block.invoke()
    }
}

/**
 * 获取该 Throwable 的堆栈信息，可用于打印日志。
 */
fun Throwable?.toLogString(): String = Log.getStackTraceString(this)

fun Bundle?.orEmpty() = this ?: Bundle()

/**
 * 模拟三目运算符操作。
 */
fun <T> ternaryOperator(predicate: Boolean, first: T, second: T): T {
    return if (predicate) {
        first
    } else {
        second
    }
}

/**
 * 模拟三目运算符操作。
 */
fun <T> ternaryOperator(predicate: () -> Boolean, first: T, second: T): T {
    return if (predicate()) {
        first
    } else {
        second
    }
}

/**
 * 复制文本
 */
fun String.copyTxt(context: Context, block: () -> Unit) {
    val cm: ClipboardManager =
        context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val mClipData = ClipData.newPlainText("Label", this)
    cm.setPrimaryClip(mClipData)
    block.invoke()
}