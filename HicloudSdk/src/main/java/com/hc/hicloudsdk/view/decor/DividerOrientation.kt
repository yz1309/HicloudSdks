package com.hc.hicloudsdk.view.decor

enum class DividerOrientation {
    VERTICAL, HORIZONTAL, GRID
}