package com.hc.hicloudsdk.utils

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import androidx.annotation.RequiresPermission
import com.hc.hicloudsdk.bean.SmsDataBean
import java.text.SimpleDateFormat
import java.util.*

/**
 * 短信工具类
 * Created by slantech on 2021/08/26 9:48
 */
object PhoneSmsUtil {
    /**
     * 获取短信数据[抓取发送成功的]
     * Notice：调用之前请保证已获取读取短信权限
     * @param lastId 上传成功的最新id
     * @param configureTime 后台配置的抓取开始时间
     * @notice 保证已配置权限｛  <uses-permission android:name="android.permission.READ_SMS" />｝
     * @return
     */
    @RequiresPermission(android.Manifest.permission.READ_SMS)
    fun getSMS(
        contentResolver: ContentResolver,
        lastId: Int,
        configureTime: Long
    ): List<SmsDataBean> {
        val sId = "_id"
        val sAddress = "address"
        val sType = "type"
        val sBody = "body"
        val sDate = "date"

        val smsUriInbox = "content://sms"
        val project = arrayOf(sId, sAddress, sType, sBody, sDate)
        val selection = "$sType in (1,2) and $sId > $lastId and $sDate > $configureTime"
        var cur: Cursor? = null
        try {
            cur = contentResolver.query(Uri.parse(smsUriInbox), project, selection, null, "_id asc")
        } catch (e: Exception) {
            //e.printStackTrace()
            return emptyList()
        }

        val smsList = mutableListOf<SmsDataBean>()

        if (cur == null)
            return smsList

        try {
            if (cur.moveToFirst().not())
                return smsList

            val index_Id = cur.getColumnIndex(sId)
            val index_Address = cur.getColumnIndex(sAddress)
            val index_Body = cur.getColumnIndex(sBody)
            val index_Date = cur.getColumnIndex(sDate)
            val index_Type = cur.getColumnIndex(sType)

            do {
                if (index_Address == -1)
                    break
                val strId = cur.getInt(index_Id)
                val strAddress = cur.getString(index_Address)
                val strbody = cur.getString(index_Body)
                val longDate = cur.getLong(index_Date)
                val intType = cur.getInt(index_Type)

                val dateFormat =
                    SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val d = Date(longDate)
                val strDate = dateFormat.format(d)

                var strType = ""
                when (intType) {
                    1 -> {
                        strType = "接收"
                    }
                    2 -> {
                        strType = "发送"
                    }
                    3 -> {
                        strType = "草稿"
                    }
                    4 -> {
                        strType = "发件箱"
                    }
                    5 -> {
                        strType = "发送失败"
                    }
                    6 -> {
                        strType = "待发送列表"
                    }
                    else -> {
                        strType = "所有短信"
                    }
                }

                smsList.add(SmsDataBean(strId, strAddress, strType, strbody, strDate))

            } while (cur.moveToNext())
        } catch (e: Exception) {

        } finally {
            if (!cur.isClosed) {
                cur.close()
            }
        }

        return smsList
    }

}