package com.hc.hicloudsdk.utils

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import android.media.MediaDrm
import android.net.wifi.WifiManager
import android.os.BatteryManager
import android.os.Build
import android.os.Environment
import android.os.StatFs
import android.provider.Settings
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.text.TextUtils
import androidx.annotation.RequiresPermission
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.hc.hicloudsdk.bean.SignParam
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.InputStreamReader
import java.net.NetworkInterface
import java.nio.charset.Charset
import java.util.*

/**
 * @Author：
 * @Time： 2021/07/30
 * @Description： 设备信息工具类
 * @Sample：
 * 获取完整的设备信息｛SignParam｝DeviceUtil.getDeviceInfo(context,afAndroidId,googleAdId)
 * 获取UUID ｛String｝DeviceUtil.uuid
 * ...
 *
 * @Notice： 调用方法前，请保证已获取权限｛Manifest.permission.READ_PHONE_STATE｝
 */
object DeviceUtil {
    /**
     * 模拟器的特有文件
     */
    private val known_pipes = arrayOf("/dev/socket/qemud", "/dev/qemu_pipe")

    /**
     * 获取完整的设备信息
     * @param afAndroidId AF得到的android
     * @param googleAdId Google得到的googleAdId
     */
    @RequiresPermission(android.Manifest.permission.READ_PHONE_STATE)
    fun getDeviceInfo(context: Context, afAndroidId: String, googleAdId: String): SignParam {
        val phoneInfo = SignParam()
        phoneInfo.imei = getIMEI(context)
        phoneInfo.imsi = getIMSI(context)
        phoneInfo.model = "$phoneBrand#$phoneModel"
        phoneInfo.deviceModel = "$phoneBrand#$phoneModel"
        phoneInfo.deviceMac = getDeviceMac(context)
        phoneInfo.uuid = uuid
        phoneInfo.uuidOriginal = uuidOriginal

        // 返回的有可能是null字符串
        if (TextUtils.isEmpty(afAndroidId) || afAndroidId == "null") {
            phoneInfo.androidId = getAndroidId(context)
        } else {
            phoneInfo.androidId = afAndroidId
        }
        phoneInfo.detailAddress = ""
        phoneInfo.locationCity = ""
        phoneInfo.resolutionHeight = getScreenHeights(context).toString()
        phoneInfo.resolutionWidth = getScreenWidths(context).toString()
        //获取手机分辨率
        phoneInfo.displayMetrics = String.format(
            Locale.getDefault(),
            "%d,%d",
            getScreenWidths(context),
            getScreenHeights(context)
        )
        phoneInfo.os = systemVersion
        phoneInfo.adbEnabled = if (getEnableAdb(context)) 1 else 0
        phoneInfo.runtimeAvailableMemory = getAvailableRuntime(context)
        phoneInfo.runtimeMaxMemory = getTotalRuntime(context)
        phoneInfo.phoneAvailableRam = getAvailMemory(context)
        phoneInfo.phoneTotalRam = getTotalMemory(context)
        phoneInfo.fingerPrint = fingerPrint
        //电量
        phoneInfo.batteryPercent = getBattery(context)
        phoneInfo.bluetoothEquipmentName = blueToothName
        phoneInfo.gaid = googleAdId
        phoneInfo.networkType = NetWorkUtil.getNetworkState(context)
        phoneInfo.timeZone = currentTimeZone
        phoneInfo.simulator = if (checkIsVirtualMachine(context)) 1 else 0
        phoneInfo.simState = if (hasSimCard(context)) 1 else 0
        phoneInfo.rooted = if (isRooted) 1 else 0
        phoneInfo.serialNumber = serialNumber
        phoneInfo.deviceVersionType = type
        phoneInfo.totalStorage = totalInternalMemorySize
        phoneInfo.availableStorage = availableInternalMemorySize

        //wifi
        phoneInfo.routerMac = NetWorkUtil.getWifiMac(context)
        phoneInfo.wifiName = NetWorkUtil.getWifiName(context)

        // 2021.09.28 add
        phoneInfo.codeName = getSystemCodeName()
        phoneInfo.cpu = getCpu()
        phoneInfo.cpuInfo = getCpuInfo()
        phoneInfo.cpuSpeed = curCpuFreq
        phoneInfo.deviceSoftwareVersion = getDeviceSoftwareVersion(context)
        phoneInfo.hardware = getHardware()
        phoneInfo.language = getLanguage(context)
        phoneInfo.networkOperator = getNetworkOperator(context)
        phoneInfo.networkOperatorName = getNetworkOperatorName(context)
        phoneInfo.product = getProduct()
        phoneInfo.radioVersion = getRadioVersion()

        getSimInfo(context).apply {
            phoneInfo.phonecardCount = this[0] as Int
            phoneInfo.operatorName = this[1] as String
        }

        return phoneInfo
    }

    /**
     * 得到手机品牌
     *
     * @return ignore
     */
    val phoneBrand: String
        get() = Build.MANUFACTURER

    /**
     * 得到手机型号
     *
     * @return ignore
     */
    val phoneModel: String
        get() {
            var model = Build.MODEL
            try {
                model = model?.trim { it <= ' ' }?.replace("\\s*".toRegex(), "") ?: ""
            } catch (e: Exception) {
            }

            if (model == null)
                model = ""

            return model
        }


    /**
     * 获取当前手机系统版本号
     *
     * @return 系统版本号
     */
    val systemVersion: String
        get() = Build.VERSION.RELEASE

    /**
     * IMEI （唯一标识序列号）
     *
     * 需与[.isPhone]一起使用
     *
     * 需添加权限 `<uses-permission android:name="android.permission.READ_PHONE_STATE"/>`
     *
     * @param context 上下文
     * @return IMEI ignore
     */
    @RequiresPermission(android.Manifest.permission.READ_PHONE_STATE)
    fun getIMEI(context: Context?): String? {
        var deviceId: String? = ""
        try {
            deviceId = if (isPhone(context)) {
                getDeviceIdIMEI(context)
            } else {
                getAndroidId(context)
            }
            if (deviceId == null) deviceId = ""
        } catch (e: Exception) {
        }
        return deviceId
    }

    /**
     * 判断设备是否是手机
     *
     * @param context 上下文
     * @return `true`: 是<br></br>`false`: 否
     */
    private fun isPhone(context: Context?): Boolean {
        val rtn = false

        try {
            val tm = context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (tm != null) {
                return tm.phoneType != TelephonyManager.PHONE_TYPE_NONE
            }
        } catch (e: Exception) {
        }

        return rtn

    }

    /**
     * 判断是否包含SIM卡
     *
     * @return 状态
     */
    fun hasSimCard(context: Context?): Boolean {
        try {
            val telMgr = context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (telMgr != null) {
                return when (telMgr.simState) {
                    TelephonyManager.SIM_STATE_ABSENT, TelephonyManager.SIM_STATE_UNKNOWN -> false
                    else -> true
                }
            }
        } catch (e: Exception) {
        }
        return true
    }

    /**
     * 获取设备的IMEI
     *
     * @param context ignore
     * @return ignore
     * @date 2020-11-11 android10 开始无法获取
     */
    @RequiresPermission(android.Manifest.permission.READ_PHONE_STATE)
    private fun getDeviceIdIMEI(context: Context?): String? {
        var imei = ""
        try {
            val tm = context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                ?: return ""
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                return ""
            }
            imei = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                tm.imei
            } else {
                tm.deviceId
            }
            if (imei == null) imei = ""
        } catch (e: Exception) {
            //android 10 throw SecurityException
            //e.printStackTrace()
        }
        return imei
    }

    /**
     * The type of build, like "user" or "eng".
     *
     * @return ignore
     */
    val type: String
        get() = Build.TYPE

    /**
     * 获取ANDROID ID
     *
     * @param context ignore
     * @return ignore
     */
    @SuppressLint("HardwareIds")
    fun getAndroidId(context: Context?): String? {
        var rtn: String? = ""
        try {
            rtn = Settings.Secure.getString(context?.contentResolver, Settings.Secure.ANDROID_ID)

            if ("9774d56d682e549c" == rtn)
                rtn = ""

            if (rtn == null)
                rtn = ""
        } catch (e: Exception) {
        }
        return rtn
    }

    /**
     * 获取蓝牙名称
     * update log：2021-08-12 fix｛PhoneEquipmentUtil.getBlueToothName() must not be null｝
     *
     * @return
     */
    val blueToothName: String?
        get() {
            var blueToothName = ""
            try {
                val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter() ?: return ""
                blueToothName = bluetoothAdapter.name
                if (blueToothName == null) {
                    return ""
                }
            } catch (e: Exception) {
            }
            return blueToothName
        }

    /**
     * 得到设备屏幕的宽度
     */
    fun getScreenWidths(context: Context?): Int {
        context?.let {
            return it.resources.displayMetrics.widthPixels
        }
        return 0
    }

    /**
     * 得到设备屏幕的高度
     */
    fun getScreenHeights(context: Context?): Int {
        context?.let {
            return it.resources.displayMetrics.heightPixels
        }
        return 0
    }

    /**
     * 判断手机是否已开启Usb
     *
     * @param context ignore
     * @return ignore
     */
    fun getEnableAdb(context: Context?): Boolean {
        context?.let {
            return Settings.Secure.getInt(
                it.contentResolver,
                Settings.Global.ADB_ENABLED,
                0
            ) > 0
        }
        return false
    }

    /**
     * 手机Fingerprint标识
     */
    val fingerPrint: String
        get() = Build.FINGERPRINT

    /**
     * 获取手机序列号
     * 方式1：执行adb 命令获得//below android 8.0// 传音手机 Android 9 不能获取此属性
     * android O(安卓 8.0)
     * android O_MR1(安卓8.1)、安卓9
     * 版本>=android Q（安卓10）
     * @return 手机序列号
     * @date 2020-11-11 android10 开始无法获取
     * @notice 需要{READ_PHONE_STAT权限}
     */
    val serialNumber: String
        @SuppressLint("MissingPermission")
        get() {
            var serial = ""
            try {
                // 版本>=android Q（安卓10）
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    return serial
                    // android O_MR1(安卓8.1)、安卓9
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                    if (Build.BRAND.toUpperCase() == "TECNO" || Build.BRAND.toUpperCase() == "ITEL" || Build.BRAND.toUpperCase() == "INFINIX") {
                        // 传音手机 Android 9 不能获取此属性
                    } else {
                        serial = Build.getSerial()
                    }
                    // android O(安卓 8.0)
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    serial = Build.SERIAL
                    //below android 8.0
                } else {
                    // 方式1：执行adb 命令获得
                    val runtime = Runtime.getRuntime()
                    var process: Process? = null
                    process = runtime.exec("getprop ro.serialno")
                    val inputStream = process.inputStream
                    val isr = InputStreamReader(inputStream)
                    val br = BufferedReader(isr)
                    serial = br.readLine()
                }
            } catch (e: Exception) {
            }

            if (serial == null)
                serial = ""

            return serial
        }

    /**
     * 获取当前时区
     *
     * @return ignore
     */
    val currentTimeZone: String
        get() {
            var strTz = ""
            try {
                val tz = TimeZone.getDefault()
                if (null != tz) {
                    strTz = tz.getDisplayName(false, TimeZone.SHORT)
                }
                return strTz
            } catch (ex: Exception) {
            }
            return strTz
        }

    /**
     * 检车设备是否是虚拟机
     *
     * @param context ignore
     * @return ignore
     */
    fun checkIsVirtualMachine(context: Context): Boolean {
        return (notHasBlueTooth()
                || notHasLightSensorManager(context)
                || isFeatures
                || checkIsNotRealPhone()
                || checkPipes())
    }

    /**
     * 判断蓝牙是否有效来判断是否为模拟器
     *
     * @return true 为模拟器
     */
    fun notHasBlueTooth(): Boolean {
        var rtn = true
        try {
            val ba = BluetoothAdapter.getDefaultAdapter()

            if (ba != null) {
                // 如果有蓝牙不一定是有效的。获取蓝牙名称，若为null 则默认为模拟器
                val name = ba.name
                rtn = TextUtils.isEmpty(name)
            }
        } catch (e: Exception) {
        }

        return rtn
    }

    /**
     * 依据是否存在光传感器来判断是否为模拟器
     *
     * @param context ignore
     * @return true 为模拟器
     */
    fun notHasLightSensorManager(context: Context?): Boolean {
        return try {
            val sensorManager = context?.getSystemService(Context.SENSOR_SERVICE) as SensorManager
            if (null != sensorManager) {
                val sensor8 = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
                return null == sensor8
            }
            true
        } catch (ignore: Exception) {
            false
        }
    }

    /**
     * 根据部分特征参数设备信息来判断是否为模拟器
     *
     * @return true 为模拟器
     */
    val isFeatures: Boolean
        get() = (Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.toLowerCase().contains("vbox")
                || Build.FINGERPRINT.toLowerCase().contains("test-keys")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk" == Build.PRODUCT)

    /**
     * 根据CPU是否为电脑来判断是否为模拟器
     *
     * @return true 为模拟器
     */
    fun checkIsNotRealPhone(): Boolean {
        val cpuInfo = readCpuInfo()
        return cpuInfo.contains("intel") || cpuInfo.contains("amd")
    }

    /**
     * 根据CPU是否为电脑来判断是否为模拟器(子方法)
     *
     * @return String
     */
    fun readCpuInfo(): String {
        var result = ""
        try {
            val args = arrayOf("/system/bin/cat", "/proc/cpuinfo")
            val cmd = ProcessBuilder(*args)
            val process = cmd.start()
            val stringBuilder = StringBuilder()
            var readLine: String? = ""
            val responseReader =
                BufferedReader(InputStreamReader(process.inputStream, Charset.forName("UTF-8")))
            while (responseReader.readLine().also { readLine = it } != null) {
                stringBuilder.append(readLine)
            }
            responseReader.close()
            result = stringBuilder.toString().toLowerCase()
        } catch (ignore: Exception) {
        }

        if (result == null)
            result = ""

        return result
    }

    /**
     * 检测模拟器的特有文件
     *
     * @return true 为模拟器
     */
    fun checkPipes(): Boolean {
        for (pipes in known_pipes) {
            val qemuSocket = File(pipes)
            if (qemuSocket.exists()) {
                return true
            }
        }
        return false
    }

    /**
     * Is rooted boolean.
     *  nexus 5x "/su/bin/"
     * @return the boolean
     */
    val isRooted: Boolean
        get() {
            // nexus 5x "/su/bin/"
            val paths = arrayOf(
                "/system/bin/",
                "/system/xbin/",
                "/sbin/",
                "/system/sd/xbin/",
                "/system/bin/failsafe/",
                "/data/local/xbin/",
                "/data/local/bin/",
                "/data/local/",
                "/system/sbin/",
                "/usr/bin/",
                "/vendor/bin/",
                "/su/bin/"
            )
            try {
                for (i in paths.indices) {
                    val path = paths[i] + "su"
                    if (File(path).exists().not())
                        continue

                    val execResult = exec(arrayOf("ls", "-l", path))
                    return !TextUtils.isEmpty(execResult) && execResult.indexOf("root") != execResult.lastIndexOf(
                        "root"
                    )
                }
            } catch (e: Exception) {
            }
            return false
        }

    private fun exec(exec: Array<String>): String {
        var ret = ""
        val processBuilder = ProcessBuilder(*exec)
        var bufferedReader: BufferedReader? = null
        try {
            val process = processBuilder.start()
            bufferedReader = BufferedReader(InputStreamReader(process.inputStream))
            var line: String
            while (bufferedReader.readLine().also { line = it } != null) {
                ret += line
            }
            process.inputStream.close()
            process.destroy()
        } catch (e: Exception) {
        } finally {
            FileUtil.closeIO(bufferedReader)
        }
        return ret
    }

    /**
     * 获取android当前可用运行内存大小
     * @param context
     * update log return null change return ""，getUnit
     */
    fun getAvailMemory(context: Context?): String? {
        try {
            val am = context?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val mi = ActivityManager.MemoryInfo()
            if (null != am) {
                am.getMemoryInfo(mi)
                // mi.availMem; 当前系统的可用内存，将获取的内存大小规格化
                //return Formatter.formatFileSize(context, mi.availMem);
                return getUnit(mi.availMem.toFloat())
            }
        } catch (e: Exception) {
        }
        return ""
    }

    /**
     * 获取android总运行内存大小
     * @param context
     * @update log：fix arrayOfString length = 1
     * *
     */
    fun getTotalMemory(context: Context?): String {
        // 系统内存信息文件
        val cmd = "/proc/meminfo"
        val value: String
        val arrayOfString: Array<String>
        var initialMemory: Long = 0
        var localFileReader: FileReader? = null
        var localBufferedReader: BufferedReader? = null
        try {
            localFileReader = FileReader(cmd)
            localBufferedReader = BufferedReader(localFileReader, 8192)
            // 读取mem info第一行，系统总内存大小
            value = localBufferedReader.readLine()
            arrayOfString = value.split("\\s+").toTypedArray()

            if (arrayOfString.size == 1) {
                initialMemory = arrayOfString[0].filter { it.isDigit() }.toLong()
            } else {
                // 获得系统总内存，单位是KB
                initialMemory = arrayOfString[1].filter { it.isDigit() }.toLong()
            }

            localBufferedReader.close()
        } catch (ignore: Exception) {
            return ""
        } finally {
            FileUtil.closeIO(localFileReader)
            FileUtil.closeIO(localBufferedReader)
        }
        // Byte转换为KB或者MB，内存大小规格化
        //return Formatter.formatFileSize(context, initialMemory)
        return getUnit(initialMemory * 1024f)
    }

    /**
     * 获取Runtime 最大内存
     */
    fun getTotalRuntime(context: Context?): String {
        val runtime = Runtime.getRuntime()
        //return (runtime.maxMemory() / 1048576L).toString() + "MB"
        return getUnit(runtime.maxMemory().toFloat())
    }

    /**
     * 获取Runtime 可用内存
     */
    fun getAvailableRuntime(context: Context?): String {
        val runtime = Runtime.getRuntime()
        val usedMemInMB = (runtime.totalMemory() - runtime.freeMemory())
        val maxHeapSizeInMB = runtime.maxMemory()
        return getUnit((maxHeapSizeInMB - usedMemInMB).toFloat())
    }

    /**
     * 获取当前cpu 频率
     *
     * @return ignore
     */
    val curCpuFreq: String
        get() {
            var result = "N/A"
            var fileReader: FileReader? = null
            var bufferedReader: BufferedReader? = null
            try {
                fileReader = FileReader("/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq")
                bufferedReader = BufferedReader(fileReader)
                val text = bufferedReader.readLine()
                result = text.trim { it <= ' ' }
                result = String.format("%sKHz", result)
            } catch (e: Exception) {
            } finally {
                FileUtil.closeIO(fileReader, bufferedReader)
            }
            return result
        }

    /**
     * 获取手机设备的mac address
     *
     * @param context ignore
     * @return ignore
     */
    @SuppressLint("HardwareIds")
    fun getDeviceMac(context: Context?): String? {
        var macAddress = "02:00:00:00:00:00"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            var interfaces: List<NetworkInterface?>? = null
            try {
                interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
                for (networkInterface in interfaces) {
                    if (networkInterface == null)
                        continue
                    if (TextUtils.isEmpty(networkInterface.name))
                        continue

                    if ("wlan0".equals(networkInterface.name, ignoreCase = true)) {
                        val macBytes = networkInterface.hardwareAddress
                        if (macBytes != null && macBytes.isNotEmpty()) {
                            val str = StringBuilder()
                            for (b in macBytes) {
                                str.append(String.format("%02X:", b))
                            }
                            if (str.isNotEmpty()) {
                                str.deleteCharAt(str.length - 1)
                            }
                            macAddress = str.toString()
                            return macAddress
                        }
                    }
                }
            } catch (e: Exception) {
            }
        } else {
            context?.let {
                try {
                    val wifi =
                        context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
                    if (wifi != null) {
                        val info = wifi.connectionInfo
                        if (info != null) {
                            macAddress = info.macAddress
                            return macAddress
                        }
                    }
                } catch (e: Exception) {
                }
            }
        }
        return macAddress
    }

    /**
     * 获取手机IMSI
     */
    @RequiresPermission(android.Manifest.permission.READ_PHONE_STATE)
    fun getIMSI(context: Context?): String? {
        var imsi = ""
        try {
            val telephonyManager =
                context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (null != telephonyManager) {
                //获取IMSI号
                imsi = telephonyManager.subscriberId
            }
            if (imsi == null) imsi = ""
        } catch (e: Exception) {
        }
        return imsi
    }

    /**
     * 获取手机内部空间总大小,不包含操作系统
     *
     * @return 大小，字节为单位
     */
    val totalInternalMemorySize: String
        get() {
            try { //获取内部存储根目录
                val path = Environment.getDataDirectory()
                //系统的空间描述类
                val stat = StatFs(path.path)
                //每个区块占字节数
                val blockSize = stat.blockSizeLong
                //区块总数
                val totalBlocks = stat.blockCountLong
                return getUnit((totalBlocks * blockSize).toFloat())
            } catch (e: Exception) {
            }
            return ""
        } //获取可用区块数量

    /**
     * 获取手机内部可用空间大小,不包含操作系统
     *
     * @return 大小，字节为单位
     */
    val availableInternalMemorySize: String
        get() {
            try {
                val path = Environment.getDataDirectory()
                val stat = StatFs(path.path)
                var blockSize: Long
                //获取可用区块数量
                var availableBlocks: Long
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    blockSize = stat.blockSizeLong
                    availableBlocks = stat.availableBlocksLong
                } else {
                    blockSize = stat.blockSize.toLong()
                    availableBlocks = stat.availableBlocks.toLong()
                }
                return getUnit((blockSize * availableBlocks).toFloat())
            } catch (e: Exception) {
            }
            return ""
        }


    //private val STORAGE_UNIT = arrayOf("B", "KB", "MB", "GB", "TB")

    /**
     * 字节转化为GB的单位
     */
    private const val GB: Long = 0x40000000

    /**
     * 单位转换
     */
    private fun getUnit(size: Float): String {
        /*var size = size
        var index = 0
        while (size > 1024 && index < 4) {
            size = size / 1024
            index++
        }
        return String.format(
            Locale.getDefault(), "%.2f%s", size,
            STORAGE_UNIT[index]
        )*/

        /*int index = 0;
        while (size > 1024 && index < 4) {
            size = size / 1024;
            index++;
        }
        return String.format(Locale.getDefault(), "%.2f%s", size, STORAGE_UNIT[index]);*/
        return String.format(
            Locale.US, "%.4f%s", size / GB, "GB"
        )
    }

    /**
     * 获取手机唯一设备id（uuid）
     * https://source.android.google.cn/devices/drm?hl=zh-cn
     * 荣耀设备基本获取不到值
     * @Time： 2021-08-11
     */
    val uuid: String
        get() {
            var uniqueId = ""
            try {
                val wideVineUuid = UUID(-0x121074568629b532L, -0x5c37d8232ae2de13L)
                var wideVineMdD: MediaDrm? = null
                try {
                    wideVineMdD = MediaDrm(wideVineUuid)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (null == wideVineMdD) {
                    return ""
                }
                val bytes = wideVineMdD.getPropertyByteArray(MediaDrm.PROPERTY_DEVICE_UNIQUE_ID)
                val sb = StringBuilder()
                for (aByte in bytes) {
                    sb.append(String.format("%02x", aByte))
                }
                uniqueId = sb.toString()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    wideVineMdD.close()
                } else {
                    wideVineMdD.release()
                }
            } catch (e: Exception) {
            }
            return uniqueId
        }

    /**
     * 获取手机唯一设备id（uuid） 原始byte[]
     * @Time： 2021-08-11
     */
    val uuidOriginal: String
        get() {
            var uniqueIdStr: String? = ""
            try {
                val wideVineUuid = UUID(-0x121074568629b532L, -0x5c37d8232ae2de13L)
                var wideVineMdD: MediaDrm? = null
                try {
                    wideVineMdD = MediaDrm(wideVineUuid)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (null == wideVineMdD) {
                    return ""
                }
                val bytes = wideVineMdD.getPropertyByteArray(MediaDrm.PROPERTY_DEVICE_UNIQUE_ID)
                if (bytes != null) {
                    uniqueIdStr = Arrays.toString(bytes)
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    wideVineMdD.close()
                } else {
                    wideVineMdD.release()
                }
            } catch (e: Exception) {
            }
            if (uniqueIdStr == null) {
                uniqueIdStr = ""
            }
            return uniqueIdStr
        }

    /**
     * 获取电量信息
     */
    @SuppressLint("NewApi")
    fun getBattery(context: Context?): String? {
        var rtn = ""
        try {
            var batteryManager =
                context?.let { ContextCompat.getSystemService(it, BatteryManager::class.java) }
            if (batteryManager != null) {
                var battery =
                    batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)

                rtn = battery.toString()
            }
        } catch (e: Exception) {
        }

        if (rtn == null) {
            rtn = ""
        }
        return rtn
    }


    /**
     * 获取系统开发代码
     */
    fun getSystemCodeName(): String? {
        return Build.VERSION.CODENAME.orEmpty()
    }

    /**
     * 获取系统开发指令集名称
     */
    fun getCpu(): String? {
        return Build.CPU_ABI.orEmpty()
    }


    /**
     * 获取系统默认的语言
     */
    fun getLanguage(context: Context?): String? {
        var language = ""
        try {
            val locale = context?.applicationContext?.resources?.configuration?.locale
            language = locale?.language.orEmpty()
        } catch (e: Exception) {
        }

        if (language == null)
            language = ""

        return language
    }

    /**
     * 获取CPU型号
     */
    fun getCpuInfo(): String? {
        var cpuInfo = ""
        val cmd = "/proc/cpuinfo"
        var result = ""
        try {
            val fr = FileReader(cmd)
            val localBufferedReader = BufferedReader(fr)
            while (localBufferedReader.readLine().also { result = it } != null) {
                if (result.contains("Hardware")) {
                    cpuInfo = result.split(":".toRegex()).toTypedArray()[1]
                }
            }
            localBufferedReader.close()
        } catch (e: Exception) {
        }

        if (cpuInfo == null) {
            return ""
        }

        return cpuInfo
    }

    /**
     * 返回手机设备的版本号
     * 移动终端的软件版本
     * 例如：GSM手机的IMEI/SV码。
     * eg:08、00
     */
    @SuppressLint("MissingPermission")
    fun getDeviceSoftwareVersion(context: Context?): String? {
        var deviceSoftwareVersion = ""
        try {
            val telephonyManager =
                context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

            if (telephonyManager != null) {
                deviceSoftwareVersion = telephonyManager.deviceSoftwareVersion.orEmpty()
            }
        } catch (e: Exception) {
        }

        if (deviceSoftwareVersion == null)
            return ""

        return deviceSoftwareVersion
    }

    /**
     * 返回当前注册运营商的数字名称（MCC+MNC）
     */
    fun getNetworkOperator(context: Context?): String? {
        var networkOperator = ""
        try {
            val telephonyManager =
                context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (telephonyManager != null)
                networkOperator = telephonyManager.networkOperator
        } catch (e: Exception) {
        }

        if (networkOperator == null)
            return ""

        return networkOperator
    }

    /**
     * 返回当前注册运营商的字符串
     */
    fun getNetworkOperatorName(context: Context?): String? {
        var networkOperatorName = ""
        try {
            val telephonyManager =
                context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            if (telephonyManager != null)
                networkOperatorName = telephonyManager.networkOperatorName
        } catch (e: Exception) {
        }

        if (networkOperatorName == null)
            return ""

        return networkOperatorName
    }

    /**
     * 获取无线电固件版本
     */
    fun getRadioVersion(): String? {
        return Build.getRadioVersion().orEmpty()
    }

    /**
     * 获取设备硬件名称,一般和基板名称一样
     */
    fun getHardware(): String? {
        return Build.HARDWARE.orEmpty()
    }

    /**
     * 获取整个产品的名称
     */
    fun getProduct(): String? {
        return Build.PRODUCT.orEmpty()
    }

    /**
     * 获取手机的SIM卡
     */
    @SuppressLint("NewApi", "MissingPermission")
    fun getSimInfo(context: Context?): Array<Any> {
        try {
            val subscriptionManager =
                (context?.getSystemService(AppCompatActivity.TELEPHONY_SUBSCRIPTION_SERVICE) as SubscriptionManager)
            val mSubInfoList = subscriptionManager.activeSubscriptionInfoList

            val s = StringBuilder("")
            mSubInfoList?.forEach {
                if (it.carrierName.isNullOrBlank().not()) {
                    if (s.isNotBlank()) {
                        s.append(",")
                    }
                    s.append(it.carrierName)
                }
            }
            return arrayOf(mSubInfoList?.size ?: 0, s.toString())
        } catch (e: Exception) {
        }

        return arrayOf(0, "")
    }

}
