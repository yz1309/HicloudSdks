package com.hc.hicloudsdk.utils

import java.text.SimpleDateFormat
import java.util.*


/**
 * @Author：
 * @Time： 2021/07/30
 * @Description：时间格式
 */
object DateUtil {
    /**
     * 日期formatter
     */
    val SECOND_FORMATTER = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)

    /**
     * 日期formatter
     */
    val MONTH_FORMATTER = SimpleDateFormat("yyyy-MM", Locale.US)

    /**
     * 日期formatter
     */
    val DAY_FORMATTER = SimpleDateFormat("yyyy-MM-dd", Locale.US)

    /**
     * 日期formatter
     */
    val MIN_FORMATTER = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US)
}
