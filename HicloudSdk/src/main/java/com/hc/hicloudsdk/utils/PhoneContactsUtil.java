package com.hc.hicloudsdk.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.hc.hicloudsdk.bean.PhoneContact;

import java.util.ArrayList;

import androidx.annotation.RequiresPermission;
import androidx.fragment.app.Fragment;

/**
 * @Author:
 * @Time: 2021/07/29
 * @Description： 获取联系人工具类
 * @Sample: 获取手机所有联系人{ArrayList<PhoneContact>}：PhoneContactsUtil.getAllContacts(this)
 * 获取手机所有联系人{String[]}：PhoneContactsUtil.getAllContacts(this,PhoneContactsUtil.contentUri)
 * @Notice： 调用方法前，请保证已获取权限｛Manifest.permission.READ_CONTACTS｝
 */
public class PhoneContactsUtil {

    public static Uri contentUri = ContactsContract.Contacts.CONTENT_URI;

    public static Uri contentUri2 = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

    /**
     * 选择联系人
     *
     * @param requestCode
     */
    public static Boolean chooseContacts(Fragment fragment, Integer requestCode) {
        Boolean rtn = false;
        try {
            Intent intent1 = new Intent();
            intent1.setAction(Intent.ACTION_PICK);
            intent1.setData(contentUri);
            fragment.startActivityForResult(intent1, requestCode);
            rtn = true;
        } catch (Exception e1) {
            e1.printStackTrace();
            try {
                Intent intent2 = new Intent();
                intent2.setAction(Intent.ACTION_PICK);
                intent2.setData(contentUri2);
                fragment.startActivityForResult(intent2, requestCode);
                rtn = true;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        return rtn;
    }

    /**
     * 选择联系人
     *
     * @param requestCode
     */
    public static Boolean chooseContacts(Activity activity, Integer requestCode) {
        Boolean rtn = false;
        try {
            Intent intent1 = new Intent();
            intent1.setAction(Intent.ACTION_PICK);
            intent1.setData(contentUri);
            activity.startActivityForResult(intent1, requestCode);
            rtn = true;
        } catch (Exception e1) {
            e1.printStackTrace();
            try {
                Intent intent2 = new Intent();
                intent2.setAction(Intent.ACTION_PICK);
                intent2.setData(contentUri2);
                activity.startActivityForResult(intent2, requestCode);
                rtn = true;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        return rtn;
    }

    /**
     * 获取手机所有联系人
     * @use 上传通讯录数据使用
     * @return ArrayList<PhoneContact>
     */
    @RequiresPermission(android.Manifest.permission.READ_CONTACTS)
    public static ArrayList<PhoneContact> getAllContacts(Context context) {
        ArrayList<PhoneContact> allContacts = new ArrayList<>();

        try {
            ArrayList<PhoneContact> defaultContacts = getAllContactsDefaultUri(context);
            if (!defaultContacts.isEmpty()) {
                allContacts.addAll(defaultContacts);
            }
        } catch (Exception ignore) {}

        // 获取sim卡的联系人
        try {
            ArrayList<PhoneContact> simContact = getSimContact(context, "content://icc/adn");
            if (!simContact.isEmpty()) {
                allContacts.addAll(simContact);
            }
        } catch (Exception ignore) {}

        try {
            ArrayList<PhoneContact> simContact = getSimContact(context, "content://icc/adn/subId/#");
            if (!simContact.isEmpty()) {
                allContacts.addAll(simContact);
            }
        } catch (Exception ignore) {}

        try {
            ArrayList<PhoneContact> simContact = getSimContact(context, "content://icc/sdn");
            if (!simContact.isEmpty()) {
                allContacts.addAll(simContact);
            }
        } catch (Exception ignore) {}

        try {
            ArrayList<PhoneContact> simContact = getSimContact(context, "content://icc/sdn/subId/#");
            if (!simContact.isEmpty()) {
                allContacts.addAll(simContact);
            }
        } catch (Exception ignore) {}

        try {
            ArrayList<PhoneContact> simContact = getSimContact(context, "content://icc/fdn");
            if (!simContact.isEmpty()) {
                allContacts.addAll(simContact);
            }
        } catch (Exception ignore) {}

        try {
            ArrayList<PhoneContact> simContact = getSimContact(context, "content://icc/fdn/subId/#");
            if (!simContact.isEmpty()) {
                allContacts.addAll(simContact);
            }
        } catch (Exception ignore) {}

        return allContacts;
    }

    /**
     * 获取手机所有联系人
     *
     * @return String[]
     */
    @RequiresPermission(android.Manifest.permission.READ_CONTACTS)
    public static String[] getAllContacts(Context context, Uri uri) {
        String[] contact = new String[2];
        //得到ContentResolver对象
        ContentResolver cr = context.getContentResolver();
        //取得电话本中开始一项的光标
        if (null == cr || null == uri) {
            return null;
        }
        Cursor cursor = cr.query(uri, null, null, null, null);
        if (cursor != null) {
            try {
                cursor.moveToFirst();
                //取得联系人姓名
                int nameFieldColumnIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                contact[0] = cursor.getString(nameFieldColumnIndex);
                //取得电话号码
                String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor phone = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId, null, null);
                if (phone != null) {
                    phone.moveToFirst();
                    contact[1] = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    phone.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
            }
        } else {
            return null;
        }
        return contact;
    }

    /**
     * 根据uri 获取联系人姓名、电话
     * 当调用chooseContacts后，在onActivityResult中获取已选择的数据
     * @param context
     * @param uri
     * @return
     */
    @RequiresPermission(android.Manifest.permission.READ_CONTACTS)
    public static String[] getSelectContacts(Context context, Uri uri) {
        String[] contact = getSelectContactsInner(context, uri);
        if (contact[0].equals("") && contact[1].equals("")) {
            contact = getSelectContacts7(context, uri);
        }
        return contact;
    }

    @RequiresPermission(android.Manifest.permission.READ_CONTACTS)
    private static ArrayList<PhoneContact> getAllContactsDefaultUri(Context context) {
        ArrayList<PhoneContact> allContacts = new ArrayList<>();
        try {
            //联系人的Uri，也就是content://com.android.contacts/contacts
            Uri uri = ContactsContract.Contacts.CONTENT_URI;
            //指定获取_id和display_name两列数据，display_name即为姓名
            String[] projection = new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME};
            //根据Uri查询相应的ContentProvider，cursor为获取到的数据集
            ContentResolver contentResolver = context.getContentResolver();
            Cursor cursor = contentResolver.query(uri, projection, null, null, null);

            if (cursor == null)
                return allContacts;

            if (cursor.moveToFirst()) {
                do {
                    Long id = cursor.getLong(0);
                    //获取姓名
                    String name = cursor.getString(1);
                    //指定获取NUMBER这一列数据
                    String[] phoneProjection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER};
                    //根据联系人的ID获取此人的电话号码
                    Cursor phonesCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            phoneProjection,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + id,
                            null,
                            null);

                    //因为每个联系人可能有多个电话号码，所以需要遍历
                    if (phonesCursor != null && phonesCursor.moveToFirst()) {
                        do {
                            String num = phonesCursor.getString(0);
                            PhoneContact contactsBean = new PhoneContact();
                            contactsBean.setContactName(name);
                            contactsBean.setContactPhone(num);
                            allContacts.add(contactsBean);

                        } while (phonesCursor.moveToNext());

                        phonesCursor.close();
                    }
                } while (cursor.moveToNext());
                cursor.close();
            }
        } catch (Exception e) {}

        return allContacts;
    }

    /**
     * 获取Sim卡联系人
     *
     * @param context
     * @param contentStr
     * @return ArrayList<PhoneContact>
     */
    private static ArrayList<PhoneContact> getSimContact(Context context, String contentStr) {
        // 读取SIM卡手机号,有三种可能:content://icc/contentStr || content://icc/sdn || content://icc/fdn
        // 具体查看类 IccProvider.java
        ArrayList<PhoneContact> simContactList = new ArrayList<>();
        Cursor cursor = null;
        try {
            Uri uri = Uri.parse(contentStr);
            cursor = context.getContentResolver().query(uri, null,
                    null, null, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    // 取得联系人名字
                    int nameIndex = cursor.getColumnIndex("name");
                    // 取得电话号码
                    int phoneIndex = cursor.getColumnIndex("phone");
                    String name = cursor.getString(nameIndex);
                    // 手机号
                    String phone = cursor.getString(phoneIndex);
                    PhoneContact simContact = new PhoneContact();
                    simContact.setContactName(name);
                    simContact.setContactPhone(phone);
                    simContactList.add(simContact);
                }
                cursor.close();
            }
        } catch (Exception e) {
            if (cursor != null) {
                cursor.close();
            }
        }
        return simContactList;
    }

    /**
     * 根据uri 获取联系人姓名、电话 android 7 over
     * 内部使用
     * @param context
     * @param uri
     * @return
     */
    @RequiresPermission(android.Manifest.permission.READ_CONTACTS)
    private static String[] getSelectContactsInner(Context context, Uri uri) {
        String[] contact = new String[2];
        contact[0] = "";
        contact[1] = "";

        if (null == uri)
            return contact;

        try {
            ContentResolver contentResolver1 = context.getContentResolver();
            if (null == contentResolver1) {
                return contact;
            }

            Cursor cursor = contentResolver1.query(uri, null, null, null, null);
            if (null == cursor) {
                return contact;
            }

            cursor.moveToFirst();
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            if (name != null)
                contact[0] = name;

            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            Cursor phoneCursor = contentResolver1.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId, null, null);
            if (phoneCursor != null) {
                phoneCursor.moveToFirst();
                String phone = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                if (phone != null)
                    contact[1] = phone;
                phoneCursor.close();
            }
            if (cursor != null)
                cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return contact;
    }

    /**
     * 适用于 android 7
     * 内部使用
     * @param context
     * @return
     */
    @RequiresPermission(android.Manifest.permission.READ_CONTACTS)
    private static String[] getSelectContacts7(Context context, Uri uri) {
        String[] contact = new String[2];
        contact[0] = "";
        contact[1] = "";

        try {
            ContentResolver contentResolver = context.getContentResolver();
            if (null == contentResolver) {
                return contact;
            }

            Cursor cursor = contentResolver.query(uri, null, null, null, null);
            if (null == cursor) {
                return contact;
            }

            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                if (name != null)
                    contact[0] = name;
                String phone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                if (phone != null)
                    contact[1] = phone;
            }

            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return contact;
    }
}
