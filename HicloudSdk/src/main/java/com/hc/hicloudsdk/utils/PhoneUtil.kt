package com.hc.hicloudsdk.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.annotation.RequiresPermission

/**
 * @Author:
 * @Time: 2021/08/02
 * @Description： 电话工具类
 * @Sample:
 * 拨打电话：PhoneUtil.call(this)
 * @Notice： 调用方法前，请保证已获取权限｛Manifest.permission.CALL_PHONE｝
 */

object PhoneUtil {

    @RequiresPermission(android.Manifest.permission.CALL_PHONE)
    fun call(context: Context, phone: String) {
        try {
            val intent = Intent(Intent.ACTION_DIAL)
            val data = Uri.parse("tel:" + phone)
            intent.data = data
            context.startActivity(intent)
        } catch (e: Exception) {
        }
    }
}