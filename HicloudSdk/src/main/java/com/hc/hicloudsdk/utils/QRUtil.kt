package com.hc.hicloudsdk.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PointF
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import com.hc.hicloudsdk.ext.ternaryOperator
import java.util.*

/**
 * @Author：
 * @Time： 2021/07/30
 * @Description： QR二维码工具类
 * @Sample：
 * 生成自定义二维码｛Bitmap｝QRUtil.createQRCodeBitmap(content,800,800,"UTF-8","H","1",Color.BLACK,Color.WHITE,null,0f,null));
 * .....
 */
object QRUtil {
    /**
     * 生成自定义二维码
     *
     * @param content              字符串内容
     * @param width                二维码宽度
     * @param height               二维码高度
     * @param characterSet         编码方式（一般使用UTF-8）
     * @param errorCorrectionLevel 容错率 L：7% M：15% Q：25% H：35%
     * @param margin               空白边距（二维码与边框的空白区域）
     * @param colorBlack           黑色色块
     * @param colorWhite           白色色块
     * @param logoBitmap           logo图片（传null时不添加logo）
     * @param logoPercent          logo所占百分比
     * @param bitmapBlack          用来代替黑色色块的图片（传null时不代替）
     * @return ignore
     */
    @JvmStatic
    fun createQRCodeBitmap(
        content: String?,
        width: Int,
        height: Int,
        characterSet: String,
        errorCorrectionLevel: String,
        margin: String,
        colorBlack: Int,
        colorWhite: Int,
        logoBitmap: Bitmap?,
        logoPercent: Float,
        bitmapBlack: Bitmap?
    ): Bitmap? {
        // 字符串内容判空
        var bitmapBlack = bitmapBlack
        if (TextUtils.isEmpty(content)) {
            return null
        }
        // 宽和高>=0
        return if (width < 0 || height < 0) {
            null
        } else try {
            /** 1.设置二维码相关配置,生成BitMatrix(位矩阵)对象  */
            val hints: Hashtable<EncodeHintType, String> = Hashtable<EncodeHintType, String>()
            // 字符转码格式设置
            if (!TextUtils.isEmpty(characterSet)) {
                hints[EncodeHintType.CHARACTER_SET] = characterSet
            }
            // 容错率设置
            if (!TextUtils.isEmpty(errorCorrectionLevel)) {
                hints[EncodeHintType.ERROR_CORRECTION] = errorCorrectionLevel
            }
            // 空白边距设置
            if (!TextUtils.isEmpty(margin)) {
                hints[EncodeHintType.MARGIN] = margin
            }
            /** 2.将配置参数传入到QRCodeWriter的encode方法生成BitMatrix(位矩阵)对象  */
            val bitMatrix: BitMatrix =
                QRCodeWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints)
            /** 3.创建像素数组,并根据BitMatrix(位矩阵)对象为数组元素赋颜色值  */
            if (bitmapBlack != null) {
                //从当前位图按一定的比例创建一个新的位图
                bitmapBlack = Bitmap.createScaledBitmap(bitmapBlack, width, height, false)
            }
            val pixels = IntArray(width * height)
            for (y in 0 until height) {
                for (x in 0 until width) {
                    //bitMatrix.get(x,y)方法返回true是黑色色块，false是白色色块
                    // 黑色色块像素设置
                    if (bitMatrix.get(x, y)) {
                        //图片不为null，则将黑色色块换为新位图的像素。
                        if (bitmapBlack != null) {
                            pixels[y * width + x] = bitmapBlack.getPixel(x, y)
                        } else {
                            pixels[y * width + x] = colorBlack
                        }
                    } else {
                        // 白色色块像素设置
                        pixels[y * width + x] = colorWhite
                    }
                }
            }
            /** 4.创建Bitmap对象,根据像素数组设置Bitmap每个像素点的颜色值,并返回Bitmap对象  */
            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
            /** 5.为二维码添加logo图标  */
            if (logoBitmap != null) {
                addLogo(bitmap, logoBitmap, logoPercent)
            } else bitmap
        } catch (e: WriterException) {
            e.printStackTrace()
            null
        }
    }

    /**
     * 向二维码中间添加logo图片(图片合成)
     *
     * @param srcBitmap   原图片（生成的简单二维码图片）
     * @param logoBitmap  logo图片
     * @param logoPercent 百分比 (用于调整logo图片在原图片中的显示大小, 取值范围[0,1] )
     * 原图片是二维码时,建议使用0.2F,百分比过大可能导致二维码扫描失败。
     * @return ignore
     */
    fun addLogo(srcBitmap: Bitmap?, logoBitmap: Bitmap?, logoPercent: Float): Bitmap? {
        var logoPercent = logoPercent
        if (srcBitmap == null) {
            return null
        }
        if (logoBitmap == null) {
            return srcBitmap
        }
        //传值不合法时使用0.2F
        if (logoPercent < 0f || logoPercent > 1f) {
            logoPercent = 0.2f
        }
        /** 1. 获取原图片和Logo图片各自的宽、高值  */
        val srcWidth = srcBitmap.width
        val srcHeight = srcBitmap.height
        val logoWidth = logoBitmap.width
        val logoHeight = logoBitmap.height

        /** 2. 计算画布缩放的宽高比  */
        val scaleWidth = srcWidth * logoPercent / logoWidth
        val scaleHeight = srcHeight * logoPercent / logoHeight

        /** 3. 使用Canvas绘制,合成图片  */
        val bitmap = Bitmap.createBitmap(srcWidth, srcHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawBitmap(srcBitmap, 0f, 0f, null)
        canvas.scale(scaleWidth, scaleHeight, (srcWidth / 2).toFloat(), (srcHeight / 2).toFloat())
        canvas.drawBitmap(
            logoBitmap,
            (srcWidth / 2 - logoWidth / 2).toFloat(),
            (srcHeight / 2 - logoHeight / 2).toFloat(),
            null
        )
        return bitmap
    }

    @JvmStatic
    fun createBarCode(
        context: Context?,
        contents: String?,
        desiredWidth: Int,
        desiredHeight: Int,
        displayCode: Boolean
    ): Bitmap? {
        var resultBitmap: Bitmap? = null
        val marginW = 20
        val barcodeFormat: BarcodeFormat = BarcodeFormat.CODE_128
        resultBitmap = if (displayCode) {
            val barcodeBitmap = encodeAsBitmap(
                contents, barcodeFormat,
                desiredWidth, desiredHeight
            )
            val codeBitmap = createCodeBitmap(
                contents, desiredWidth + 2
                        * marginW, desiredHeight, context
            )
            mixtureBitmap(
                barcodeBitmap, codeBitmap, PointF(
                    0f,
                    desiredHeight.toFloat()
                )
            )
        } else {
            encodeAsBitmap(
                contents, barcodeFormat,
                desiredWidth, desiredHeight
            )
        }
        return resultBitmap
    }

    fun encodeAsBitmap(
        contents: String?,
        format: BarcodeFormat?, desiredWidth: Int, desiredHeight: Int
    ): Bitmap {
        val WHITE = -0x1
        val BLACK = -0x1000000
        val writer = MultiFormatWriter()
        var result: BitMatrix? = null
        try {
            result = writer.encode(
                contents, format, desiredWidth,
                desiredHeight, null
            )
        } catch (e: WriterException) {
            e.printStackTrace()
        }
        val width: Int = ternaryOperator(result == null, 0, result!!.getWidth())
        val height: Int = result.getHeight()
        val pixels = IntArray(width * height)
        // All are 0, or black, by default
        for (y in 0 until height) {
            val offset = y * width
            for (x in 0 until width) {
                pixels[offset + x] = if (result.get(x, y)) BLACK else WHITE
            }
        }
        val bitmap = Bitmap.createBitmap(
            width, height,
            Bitmap.Config.ARGB_8888
        )
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
        return bitmap
    }

    fun createCodeBitmap(
        contents: String?, width: Int,
        height: Int, context: Context?
    ): Bitmap {
        val tv = TextView(context)
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        )
        tv.layoutParams = layoutParams
        tv.text = contents
        tv.height = height
        tv.gravity = Gravity.CENTER_HORIZONTAL
        tv.width = width
        tv.isDrawingCacheEnabled = true
        tv.setTextColor(Color.BLACK)
        tv.measure(
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        tv.layout(0, 0, tv.measuredWidth, tv.measuredHeight)
        tv.buildDrawingCache()
        return tv.drawingCache
    }

    fun mixtureBitmap(
        first: Bitmap?, second: Bitmap?,
        fromPoint: PointF?
    ): Bitmap? {
        if (first == null || second == null || fromPoint == null) {
            return null
        }
        val marginW = 20
        val newBitmap = Bitmap.createBitmap(
            first.width + second.width + marginW,
            first.height + second.height, Bitmap.Config.ARGB_4444
        )
        val cv = Canvas(newBitmap)
        cv.drawBitmap(first, marginW.toFloat(), 0f, null)
        cv.drawBitmap(second, fromPoint.x, fromPoint.y, null)
        cv.save()
        cv.restore()
        return newBitmap
    }
}