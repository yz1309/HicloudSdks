package com.hc.hicloudsdk.utils

import android.content.Context
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.os.Build
import android.util.Log

/**
 * @Author： hqs
 * @Time： 2021/07/30
 * @Description：安装包工具类
 * @Sampel：
 * 获取应用程序的包名｛String｝ PackageUtil.getPackageName(this)
 * 获取应用程序的版本号 ｛Long｝ PackageUtil.getVersionCode(this)
 * 获取应用程序的外部版本号 String｝PackageUtil.getVersionName(this)
 * 读取manifest.xml中application标签下的配置项，如果不存在，则返回空字符串 ｛String｝ PackageUtil.getConfigString(this,"appId")
 * 读取manifest.xml中application标签下的配置项 ｛Int｝ PackageUtil.getConfigInt(this,"app")
 * 读取manifest.xml中application标签下的配置项 ｛Boolean｝ PackageUtil.getConfigBoolean(this,"channel")
 */
object PackageUtil {
    private const val TAG = "PackageUtil"
    const val GOOGLE_PLAY_STORE_PACKAGE_NAME = "com.android.vending"

    /**
     * 获取应用程序的包名
     *
     * @return 应用程序的包名
     */
    fun getPackageName(context: Context): String {
        return context.packageName
    }

    /**
     * 获取应用程序的版本号
     *
     * @return 版本号
     */
    fun getVersionCode(context: Context): Long {
        var appVersionCode: Long = 0
        try {
            val packageInfo = context.applicationContext
                .packageManager
                .getPackageInfo(context.packageName, 0)
            appVersionCode =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    packageInfo.longVersionCode
                } else {
                    packageInfo.versionCode.toLong()
                }
        } catch (ignore: Exception) {
        }
        return appVersionCode
    }

    /**
     * 获取应用程序的外部版本号
     *
     * @return 外部版本号
     */
    fun getVersionName(context: Context): String {
        var versionName = ""
        try {
            versionName = context.packageManager.getPackageInfo(context.packageName, 0).versionName
        } catch (ignore: Exception) {
        }
        return versionName
    }

    /**
     * 读取manifest.xml中application标签下的配置项，如果不存在，则返回空字符串
     *
     * @param key 键名
     * @return 返回字符串
     */
    fun getConfigString(context: Context, key: String?): String {
        var metaElement = ""
        try {
            val appInfo = context.packageManager.getApplicationInfo(
                context.packageName,
                PackageManager.GET_META_DATA
            )
            metaElement = appInfo.metaData.getString(key).orEmpty()
            if (metaElement == null) {
                Log.e("PackageUtil", "please set config value for %s in manifest.xml first " + key)
            }
        } catch (ignore: Exception) {
        }
        return metaElement
    }

    /**
     * 读取manifest.xml中application标签下的配置项
     *
     * @param key 键名
     * @return 返回字符串
     */
    fun getConfigInt(context: Context, key: String?): Int {
        var metaElement = -1
        try {
            val appInfo = context.packageManager.getApplicationInfo(
                context.packageName,
                PackageManager.GET_META_DATA
            )
            metaElement = appInfo.metaData.getInt(key)
        } catch (ignore: Exception) {
        }
        return metaElement
    }

    /**
     * 读取manifest.xml中application标签下的配置项
     *
     * @param key 键名
     * @return 返回字符串
     */
    fun getConfigBoolean(context: Context, key: String?): Boolean {
        var metaElement = false
        try {
            val appInfo = context.packageManager.getApplicationInfo(
                context.packageName,
                PackageManager.GET_META_DATA
            )
            metaElement = appInfo.metaData.getBoolean(key)
        } catch (ignore: Exception) {
        }
        return metaElement
    }
}