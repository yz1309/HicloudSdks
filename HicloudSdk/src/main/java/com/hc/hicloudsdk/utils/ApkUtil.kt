package com.hc.hicloudsdk.utils

import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.net.Uri
import android.os.Build
import android.text.TextUtils
import androidx.annotation.RequiresPermission
import androidx.core.content.FileProvider
import com.hc.hicloudsdk.bean.AppListParam
import java.io.File

/**
 * @Author：
 * @Time： 2021/07/30
 * @Description： Apk工具类
 * @Sample：
 *
 * 获取手机已安装的App｛AppListParam｝ApkUtil.scanLocalInstallAppList(PackageManager)
 * @Notice： 调用方法前，请保证保证已配置权限｛｛android.permission.QUERY_ALL_PACKAGES｝
 *
 * 判断应用程序是否是用户程序｛Boolean｝ ApkUtil.filterApp(ApplicationInfo)
 *
 * 判断指定应用是否安装 ｛Boolean｝ ApkUtil.checkSpecifiedAppInstalled(this,packageName)
 *
 * 安装一个apk ｛void｝ ApkUtil.installApk(this,"",file)
 * @Notice： 调用方法前，请保证已获取权限｛Manifest.permission.REQUEST_INSTALL_PACKAGES｝
 *
 * 打开对应应用市场跳转到指定App ｛void｝ ApkUtil.openSpecifiedStoreUpdate(this,appPkg)
 *
 * 打开GooglePlayStore跳转到指定App ｛void｝ ApkUtil.openGooglePlayStoreJumpToSpecifiedApp(this,packageName)
 *
 *
 */
object ApkUtil {

    /**
     * 获取手机已安装的App
     * @param packageManager
     * @notice 保证已配置权限｛  <uses-permission android:name="android.permission.QUERY_ALL_PACKAGES" />｝
     * @return
     */
    @RequiresPermission(android.Manifest.permission.QUERY_ALL_PACKAGES)
    fun scanLocalInstallAppList(packageManager: PackageManager): List<AppListParam> {
        val installApps: MutableList<AppListParam> = ArrayList()
        var appInfoBean: AppListParam
        var installedThirdApk = ""
        var packageName = ""
        var installTime = ""
        var lastUpdateTime = ""
        try {
            val packageInfoList = packageManager.getInstalledPackages(0)
            if (packageInfoList == null || packageInfoList.size == 0) {
                return installApps
            }
            for (i in packageInfoList.indices) {
                try {
                    val packageInfo = packageInfoList[i]
                    if (packageInfo == null){
                        continue
                    }

                    if (packageInfo.applicationInfo == null)
                    {
                        continue
                    }

                    //过滤掉系统app
                    if (ApplicationInfo.FLAG_SYSTEM and packageInfo.applicationInfo.flags != 0) {
                        continue
                    }
                    installedThirdApk =
                        packageInfo.applicationInfo.loadLabel(packageManager).toString()
                    packageName = packageInfo.packageName.orEmpty()
                    if (packageInfo.applicationInfo.loadIcon(packageManager) == null) {
                        continue
                    }
                    installTime = DateUtil.SECOND_FORMATTER.format(packageInfo.firstInstallTime)
                    lastUpdateTime = DateUtil.SECOND_FORMATTER.format(packageInfo.lastUpdateTime)
                    appInfoBean = AppListParam()
                    appInfoBean.appName = installedThirdApk
                    appInfoBean.packageName = packageName
                    appInfoBean.firstInstallTime = installTime
                    appInfoBean.lastUpdateTime = lastUpdateTime
                    installApps.add(appInfoBean)
                } catch (ignore: Exception) {
                }
            }
        } catch (ignore: Exception) {
        }
        return installApps
    }

    /**
     * 判断应用程序是否是用户程序
     *
     */
    fun filterApp(info: ApplicationInfo): Boolean {
        //原来是系统应用，用户手动升级
        return if (info.flags and ApplicationInfo.FLAG_UPDATED_SYSTEM_APP != 0) {
            true
        } else info.flags and ApplicationInfo.FLAG_SYSTEM == 0
        //用户自己安装的应用程序
    }

    /**
     * 判断指定应用是否安装
     */
    fun checkSpecifiedAppInstalled(context: Context, packageName: String?): Boolean {
        var packageInfo: PackageInfo?
        try {
            packageInfo = context.packageManager.getPackageInfo(packageName.orEmpty(), 0)
        } catch (e: NameNotFoundException) {
            packageInfo = null
            e.printStackTrace()
        }
        return packageInfo != null
    }

    /**
     * 安装一个apk
     *
     * @param context     上下文
     * @param authorities Android N 授权
     * @param apk         安装包文件
     * @notice 请保证已获取权限｛Manifest.permission.REQUEST_INSTALL_PACKAGES｝
     */
    @RequiresPermission(android.Manifest.permission.REQUEST_INSTALL_PACKAGES)
    fun installApk(context: Context, authorities: String?, apk: File?) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.action = Intent.ACTION_VIEW
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        val uri: Uri
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(
                context, authorities!!,
                apk!!
            )
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        } else {
            uri = Uri.fromFile(apk)
        }
        intent.setDataAndType(uri, "application/vnd.android.package-archive")
        context.startActivity(intent)
    }

    /**
     * 打开对应应用市场跳转到指定App
     */
    fun openSpecifiedStoreUpdate(context: Context, appPkg: String, channelName: String) {
        try {
            var marketName = ""
            marketName = when (channelName) {
                "googlePlay" -> PackageUtil.GOOGLE_PLAY_STORE_PACKAGE_NAME
                else -> PackageUtil.GOOGLE_PLAY_STORE_PACKAGE_NAME
            }
            val uri = Uri.parse("market://details?id=$appPkg")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            if (!TextUtils.isEmpty(marketName)) {
                intent.setPackage(marketName)
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        } catch (ex: Exception) {
            openGooglePlayStoreJumpToSpecifiedApp(context, appPkg)
        }
    }

    /**
     * 打开GooglePlayStore跳转到指定App
     */
    fun openGooglePlayStoreJumpToSpecifiedApp(context: Context, packageName: String) {
        try {
            //用户安装了Google Play Store
            if (checkSpecifiedAppInstalled(context, PackageUtil.GOOGLE_PLAY_STORE_PACKAGE_NAME)) {
                context.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$packageName")
                    )
                )
            } else {
                context.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                    )
                )
            }
        } catch (ex: Exception) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                )
            )
        }
    }
}
