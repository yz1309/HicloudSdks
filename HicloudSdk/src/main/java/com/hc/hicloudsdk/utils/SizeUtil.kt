package com.hc.hicloudsdk.utils

import android.content.Context
import android.util.TypedValue

/**
 * @Author：
 * @Time： 2021/07/30
 * @Description： dp、px 工具转换类
 * @Sample：
 * dp 转 px｛Int｝SizeUtils.dp2px(context,2)
 * sp 转 px｛Int｝SizeUtils.sp2px(context,2)
 * px 转 sp｛Int｝SizeUtils.px2sp(context,2)
 */
object SizeUtil {

    @JvmStatic
    fun dp2px(context: Context, dpValue: Float): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dpValue,
            context.resources.displayMetrics
        )
            .toInt()
    }

    fun sp2px(context: Context, spValue: Float): Int {
        return (spValue * context.resources.displayMetrics.scaledDensity + 0.5f).toInt()
    }

    fun px2sp(context: Context, pxValue: Float): Int {
        return (pxValue / context.resources.displayMetrics.scaledDensity + 0.5f).toInt()
    }
}
