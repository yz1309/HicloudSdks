package com.hc.hicloudsdk.utils

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.telephony.TelephonyManager

/**
 * @Author：
 * @Time： 2021/07/30
 * @Description： 网络工具类
 * @Sample：
 * 判断已连接的WIFI是否可用｛Boolean｝NetWorkUtil.isWiFiAvailable(context)
 * 获取当前网络连接类型｛String｝NetWorkUtil.getNetworkState(context)
 * 获取wifi mac地址｛String｝NetWorkUtil.getWifiMac(context)
 * 获取wifi name ｛String｝NetWorkUtil.getWifiName(context)
 * @Notice： 调用方法前，请保证已获取权限｛Manifest.permission.INTERNET、Manifest.permission.ACCESS_NETWORK_STATE、Manifest.permission.ACCESS_WIFI_STATE｝
 */
object NetWorkUtil {
    /**
     * 没有网络连接
     */
    const val NETWORK_NONE = "none"

    /**
     * wifi连接
     */
    const val NETWORK_WIFI = "wifi"

    /**
     * 手机网络数据连接类型
     */
    const val NETWORK_2G = "2g"
    const val NETWORK_3G = "3g"
    const val NETWORK_4G = "4g"
    const val NETWORK_5G = "5g"
    const val NETWORK_MOBILE = "mobile"

    /**
     * 判断已连接的WIFI是否可用
     *
     * @param context ignore
     * @return ignore
     */
    fun isWiFiAvailable(context: Context?): Boolean {
        try {
            context?.let {
                val connectivityManager =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                return if (connectivityManager != null) {
                    @SuppressLint("MissingPermission")
                    val wiFiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                    val connected = wiFiInfo!!.isConnected
                    if (connected) {
                        wiFiInfo!!.isAvailable
                    } else {
                        false
                    }
                } else {
                    false
                }
            }
        } catch (e: Exception) {
        }
        return false
    }

    /**
     * 获取当前网络连接类型
     *
     * @param context ignore
     * @return ignore
     */
    @SuppressLint("MissingPermission")
    fun getNetworkState(context: Context): String {
        try {//获取系统的网络服务
            val connManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    ?: return NETWORK_NONE
            //如果当前没有网络
            //获取当前网络类型，如果为空，返回无网络
            val activeNetInfo = connManager.activeNetworkInfo
            if (activeNetInfo == null || !activeNetInfo.isAvailable) {
                return NETWORK_NONE
            }
            // 判断是不是连接的是不是wifi
            val wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            if (null != wifiInfo) {
                val state = wifiInfo.state
                if (null != state) {
                    if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                        return NETWORK_WIFI
                    }
                }
            }
            // 如果不是wifi，则判断当前连接的是运营商的哪种网络2g、3g、4g等
            val networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
            if (null != networkInfo) {
                val state = networkInfo.state
                val strSubTypeName = networkInfo.subtypeName
                if (null != state) {
                    if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                        return when (activeNetInfo.subtype) {
                            TelephonyManager.NETWORK_TYPE_GPRS, TelephonyManager.NETWORK_TYPE_CDMA, TelephonyManager.NETWORK_TYPE_EDGE, TelephonyManager.NETWORK_TYPE_1xRTT, TelephonyManager.NETWORK_TYPE_IDEN -> NETWORK_2G
                            TelephonyManager.NETWORK_TYPE_EVDO_A, TelephonyManager.NETWORK_TYPE_UMTS, TelephonyManager.NETWORK_TYPE_EVDO_0, TelephonyManager.NETWORK_TYPE_HSDPA, TelephonyManager.NETWORK_TYPE_HSUPA, TelephonyManager.NETWORK_TYPE_HSPA, TelephonyManager.NETWORK_TYPE_EVDO_B, TelephonyManager.NETWORK_TYPE_EHRPD, TelephonyManager.NETWORK_TYPE_HSPAP -> NETWORK_3G
                            TelephonyManager.NETWORK_TYPE_LTE -> NETWORK_4G
                            TelephonyManager.NETWORK_TYPE_NR -> NETWORK_5G
                            else ->                             //中国移动 联通 电信 三种3G制式
                                if (strSubTypeName.equals("TD-SCDMA", ignoreCase = true) ||
                                    strSubTypeName.equals("WCDMA", ignoreCase = true) ||
                                    strSubTypeName.equals("CDMA2000", ignoreCase = true)
                                ) {
                                    NETWORK_3G
                                } else {
                                    NETWORK_MOBILE
                                }
                        }
                    }
                }
            }
        } catch (e: Exception) {
        }
        return NETWORK_NONE
    }

    /**
     * 获取wifi mac地址
     * @Description return "02:00:00:00:00:00" ｛Android 8.1 之后，需要获取到权限 ACCESS_COARSE_LOCATION｝
     * https://developer.android.com/reference/android/net/wifi/WifiInfo#getBSSID()
     * https://developer.android.com/reference/android/net/wifi/WifiManager#getScanResults()
     *
     * <p>
     * In the connected state, access to the SSID and BSSID requires
     * the same permissions as {@link #getScanResults}. If such access is not allowed,
     * {@link WifiInfo#getSSID} will return {@link #UNKNOWN_SSID} and
     * {@link WifiInfo#getBSSID} will return {@code "02:00:00:00:00:00"}.
     * {@link WifiInfo#getPasspointFqdn()} will return null.
     * {@link WifiInfo#getPasspointProviderFriendlyName()} will return null.
     *
     * List<ScanResult>	the list of access points found in the most recent scan.
     * An app must hold ACCESS_FINE_LOCATION permission in order to get valid results.
     */
    fun getWifiMac(context: Context): String {
        var wifiMac = ""

        if (isWiFiAvailable(context).not()) {
            return wifiMac
        }
        try {
            var wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
            if (wifiManager != null) {
                val info: WifiInfo = wifiManager.connectionInfo
                if (info != null) {
                    wifiMac = info.bssid
                }
            }
        } catch (e: Exception) {
        }

        if (wifiMac == null)
            wifiMac = ""

        return wifiMac
    }

    /**
     * 获取wifi name
     */
    fun getWifiName(context: Context): String {
        var wifiName = ""

        if (isWiFiAvailable(context).not()) {
            return wifiName
        }
        try {
            var wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
            if (wifiManager != null) {
                val info: WifiInfo = wifiManager.connectionInfo
                if (info != null) {
                    wifiName = info.ssid
                }
            }
        } catch (e: Exception) {
        }

        if (wifiName == null)
            wifiName = ""

        return wifiName
    }

}