package com.hc.hicloudsdk.utils

import java.util.regex.Matcher
import java.util.regex.Pattern


object RegexUtil {
    /**
     * 是否为 匹配正则
     */
    fun isMatch(txt: String, regex: String): Boolean {
        val p: Pattern = Pattern.compile(regex)
        val m: Matcher = p.matcher(txt)
        val isMatch: Boolean = m.matches()
        return isMatch
    }
}