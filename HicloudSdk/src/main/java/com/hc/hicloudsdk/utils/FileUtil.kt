package com.hc.hicloudsdk.utils

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Build
import android.os.Environment
import java.io.*
import java.math.BigDecimal
import java.math.BigInteger
import java.security.MessageDigest

/**
 * @Author：
 * @Time： 2021/07/30
 * @Description：文件工具类
 * 获取缓存大小｛String｝FileUtil.getTotalCacheSize(context)
 * 清除缓存｛｝FileUtil.clearAllCache(context)
 * 创建Camera File对象｛File｝FileUtil.getCameraFile(context,"xx.png")
 * .......
 *
 * @Notice： 调用方法前，请保证已获取权限｛Manifest.permission.READ_EXTERNAL_STORAGE、Manifest.permission.WRITE_EXTERNAL_STORAGE｝
 */
object FileUtil {
    const val reqSquarePixels = 1920 * 1080//内部存储的根目录    /data//SD卡根目录    /storage/emulated/0

    /**
     * 得到SD卡根目录，SD卡不可用则获取内部存储的根目录
     */
    val rootPath: File?
        get() {
            var path: File? = null
            path = if (sdCardIsAvailable()) {
                Environment.getExternalStorageDirectory() //SD卡根目录    /storage/emulated/0
            } else {
                Environment.getDataDirectory() //内部存储的根目录    /data
            }
            return path
        }

    /**
     * SD卡是否可用
     */
    fun sdCardIsAvailable(): Boolean {
        return if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            val sd = File(Environment.getExternalStorageDirectory().path)
            sd.canWrite()
        } else {
            false
        }
    }

    /**
     * 判断目录是否存在，不存在则判断是否创建成功
     *
     * @param dirPath 文件路径
     * @return `true`: 存在或创建成功<br></br>`false`: 不存在或创建失败
     */
    fun createOrExistsDir(dirPath: String?): Boolean {
        return createOrExistsDir(getFileByPath(dirPath))
    }

    /**
     * 判断目录是否存在，不存在则判断是否创建成功
     *
     * @param file 文件
     * @return `true`: 存在或创建成功<br></br>`false`: 不存在或创建失败
     */
    fun createOrExistsDir(file: File?): Boolean {
        // 如果存在，是目录则返回true，是文件则返回false，不存在则返回是否创建成功
        return file != null && if (file.exists()) file.isDirectory else file.mkdirs()
    }

    /**
     * 判断文件是否存在，不存在则判断是否创建成功
     *
     * @param filePath 文件路径
     * @return `true`: 存在或创建成功<br></br>`false`: 不存在或创建失败
     */
    fun createOrExistsFile(filePath: String?): Boolean {
        return createOrExistsFile(getFileByPath(filePath))
    }

    /**
     * 判断文件是否存在，不存在则判断是否创建成功
     *
     * @param file 文件
     * @return `true`: 存在或创建成功<br></br>`false`: 不存在或创建失败
     */
    fun createOrExistsFile(file: File?): Boolean {
        if (file == null) {
            return false
        }
        // 如果存在，是文件则返回true，是目录则返回false
        if (file.exists()) {
            return file.isFile
        }
        return if (!createOrExistsDir(file.parentFile)) {
            false
        } else try {
            file.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
            false
        }
    }

    /**
     * 根据文件路径获取文件
     *
     * @param filePath 文件路径
     * @return 文件
     */
    fun getFileByPath(filePath: String?): File? {
        return if (isSpace(filePath)) null else File(filePath)
    }

    /**
     * 判断字符串是否为 null 或全为空白字符
     *
     * @param s
     * @return
     */
    private fun isSpace(s: String?): Boolean {
        if (s == null) {
            return true
        }
        var i = 0
        val len = s.length
        while (i < len) {
            if (!Character.isWhitespace(s[i])) {
                return false
            }
            ++i
        }
        return true
    }

    /**
     * 创建File对象，对应于data/data/${packageName}/cache/fileName.
     *
     * @param fileName 文件名
     * @return File
     */
    @TargetApi(Build.VERSION_CODES.FROYO)
    fun getCameraFile(context: Context, fileName: String?): File {
        val dir = File(context.externalCacheDir, "camera")
        if (!dir.exists()) {
            dir.mkdirs()
        }
        return File(dir, fileName)
    }

    /**
     * 创建File对象，对应于data/data/${packageName}/cache/fileName.
     *
     * @param fileName 文件名
     * @return File
     */
    @TargetApi(Build.VERSION_CODES.FROYO)
    fun getLivenessFile(context: Context, fileName: String?): File {
        val dir = File(context.externalCacheDir, "liveness")
        if (!dir.exists()) {
            dir.mkdirs()
        }
        return File(dir, fileName)
    }

    /**
     * 获取文件路劲
     */
    val path: String
        get() {
            val path = Environment.getExternalStorageDirectory().toString() + "/cache/"
            val file = File(path)
            return if (file.mkdirs()) {
                path
            } else path
        }

    /**
     * 保存bitmap
     *
     * @param fileName 文件名
     */
    fun saveBitmap(context: Context, bitmap: Bitmap, fileName: String): File {
        val cropFile = getCameraFile(context, "$fileName.png")
        val fos = FileOutputStream(cropFile)
        //JPEG为硬件加速，O(1)时间复杂度，而PNG为O(n)，速度要慢很多，WEBP不常用
        //90%的品质已高于超精细(85%)的标准，已非常精细
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
        fos.flush()
        bitmap.recycle()
        closeIO(fos)
        return cropFile
    }

    /**
     * 处理旋转后的图片
     *
     * @param originpath 原图路径
     * @param context    上下文
     * @return 返回修复完毕后的图片路径
     */
    fun amendRotatePhoto(originpath: String?, context: Context, angle: Int, name: String): String? {

        // 取得图片旋转角度 应该直接读取图片的旋转角度 ，但是不知为何读出来为0

        // 把原图压缩后得到Bitmap对象
        val bmp = getCompressPhoto(originpath)

        // 修复图片被旋转的角度
        val bitmap = rotateBitmap(bmp, angle)

        // 保存修复后的图片并返回保存后的图片路径
        return savePhotoToSD(bitmap, context, name)
    }

    /**
     * 把原图按1/10的比例压缩
     *
     * @param path 原图的路径
     * @return 压缩后的图片
     */
    fun getCompressPhoto(path: String?): Bitmap {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = false
        options.inSampleSize = 4 // 图片的大小设置为原来的十分之一
        return BitmapFactory.decodeFile(path, options)
    }

    /**
     * 旋转图片
     */
    fun rotateBitmap(bitmap: Bitmap?, degress: Int): Bitmap? {
        if (bitmap == null) {
            return null
        }
        val m = Matrix()
        m.postRotate(degress.toFloat())
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, m, true)
    }

    /**
     * 保存Bitmap图片在SD卡中
     * 如果没有SD卡则存在手机中
     *
     * @param mbitmap 需要保存的Bitmap图片
     * @return 保存成功时返回图片的路径，失败时返回null
     */
    fun savePhotoToSD(mbitmap: Bitmap?, context: Context, name: String): String? {
        var outStream: FileOutputStream? = null
        val fileName = getPhotoFileName(context, name)
        return try {
            outStream = FileOutputStream(fileName)
            // 把数据写入文件，100表示不压缩
            mbitmap!!.compress(Bitmap.CompressFormat.PNG, 100, outStream)
            fileName
        } catch (e: Exception) {
            e.printStackTrace()
            null
        } finally {
            closeIO(outStream)
            mbitmap?.recycle()
        }
    }

    /**
     * 使用当前系统时间作为上传图片的名称
     *
     * @return 存储的根路径+图片名称
     */
    fun getPhotoFileName(context: Context, name: String): String {
        val file =
            File(getPhoneRootPath(context) + "File")
        // 判断文件是否已经存在，不存在则创建
        if (!file.exists()) {
            file.mkdirs()
        }
        // 设置图片文件名称
        val photoName = "/" + name + "Image"
        return file.toString() + photoName
    }

    /**
     * 获取手机可存储路径
     *
     * @param context 上下文
     * @return 手机可存储路径
     */
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private fun getPhoneRootPath(context: Context): String {
        // 是否有SD卡
        return if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED || !Environment.isExternalStorageRemovable()) {
            // 获取SD卡根目录
            context.externalCacheDir!!.path
        } else {
            // 获取apk包下的缓存路径
            context.cacheDir.path
        }
    }

    /**
     * 关闭IO
     *
     * @param closeables closeable
     */
    fun closeIO(vararg closeables: Closeable?) {
        if (closeables == null) {
            return
        }
        try {
            for (closeable in closeables) {
                closeable?.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * 获取缓存大小
     *
     * @param context
     * @return ignore
     * @throws Exception
     */
    fun getTotalCacheSize(context: Context): String {
        var cacheSize = getFolderSize(context.cacheDir)
        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            cacheSize += getFolderSize(context.externalCacheDir)
        }
        return getFormatSize(cacheSize.toDouble())
    }

    /**
     * 清除缓存
     *
     * @param context
     */
    fun clearAllCache(context: Context) {
        deleteDir(context.cacheDir)
        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            deleteDir(context.externalCacheDir)
        }
    }

    private fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
        }
        return dir!!.delete()
    }

    // 获取文件大小
    //Context.getExternalFilesDir() --> SDCard/Android/data/你的应用的包名/files/ 目录，一般放一些长时间保存的数据
    //Context.getExternalCacheDir() --> SDCard/Android/data/你的应用包名/cache/目录，一般存放临时缓存数据
    fun getFolderSize(file: File?): Long {
        var size: Long = 0
        try {
            val fileList = file!!.listFiles()
            for (i in fileList.indices) {
                // 如果下面还有文件
                size = if (fileList[i].isDirectory) {
                    size + getFolderSize(fileList[i])
                } else {
                    size + fileList[i].length()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return size
    }

    /**
     * 格式化单位
     *
     * @param size
     * @return ignore
     */
    fun getFormatSize(size: Double): String {
        val kiloByte = size / 1024
        if (kiloByte < 1) {
//            return size + "Byte";
            return "0K"
        }
        val megaByte = kiloByte / 1024
        if (megaByte < 1) {
            val result1 = BigDecimal(java.lang.Double.toString(kiloByte))
            return result1.setScale(2, BigDecimal.ROUND_HALF_UP)
                .toPlainString() + "K"
        }
        val gigaByte = megaByte / 1024
        if (gigaByte < 1) {
            val result2 = BigDecimal(java.lang.Double.toString(megaByte))
            return result2.setScale(2, BigDecimal.ROUND_HALF_UP)
                .toPlainString() + "M"
        }
        val teraBytes = gigaByte / 1024
        if (teraBytes < 1) {
            val result3 = BigDecimal(java.lang.Double.toString(gigaByte))
            return result3.setScale(2, BigDecimal.ROUND_HALF_UP)
                .toPlainString() + "GB"
        }
        val result4 = BigDecimal(teraBytes)
        return (result4.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString()
                + "TB")
    }

    /**
     * 创建一个文件
     *
     * @param downloadPath 路径
     * @param fileName     名字
     * @return 文件
     */
    fun createFile(downloadPath: String?, fileName: String?): File {
        return File(downloadPath, fileName)
    }

    /**
     * 查看一个文件是否存在
     *
     * @param downloadPath 路径
     * @param fileName     名字
     * @return true | false
     */
    fun fileExists(downloadPath: String?, fileName: String?): Boolean {
        return File(downloadPath, fileName).exists()
    }

    /**
     * 删除一个文件
     *
     * @param downloadPath 路径
     * @param fileName     名字
     * @return true | false
     */
    fun delete(downloadPath: String?, fileName: String?): Boolean {
        return File(downloadPath, fileName).delete()
    }

    /**
     * 获取一个文件的MD5
     *
     * @param file 文件
     * @return MD5
     */
    fun getFileMD5(file: File?): String {
        return if (file == null || !file.exists()) {
            ""
        } else try {
            val buffer = ByteArray(1024)
            var len: Int
            val digest = MessageDigest.getInstance("MD5")
            val `in` = FileInputStream(file)
            while (`in`.read(buffer).also { len = it } != -1) {
                digest.update(buffer, 0, len)
            }
            `in`.close()
            val bigInt = BigInteger(1, digest.digest())
            bigInt.toString(16).toUpperCase()
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }

    /**
     * 创建保存的文件夹
     */
    fun createDirDirectory(downloadPath: String?) {
        val dirDirectory = File(downloadPath)
        if (!dirDirectory.exists()) {
            dirDirectory.mkdirs()
        }
    }
}