package com.hc.hicloudsdk.utils

import android.Manifest
import android.content.ContentResolver
import android.database.Cursor
import android.media.ExifInterface
import android.media.ExifInterface.TAG_MAKE
import android.media.ExifInterface.TAG_MODEL
import android.net.Uri
import android.os.Build
import android.os.Build.VERSION_CODES.Q
import android.provider.MediaStore
import android.util.Log
import com.hc.hicloudsdk.bean.PicDataParam
import com.hc.hicloudsdk.ext.ternaryOperator
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * @Author：liwei
 * @Time：2021/9/23
 * @Description：获取设备图片数据
 *
 */
object AlbumUtil {

    var permissions = mutableListOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
        get() {
            if (Build.VERSION.SDK_INT >= Q) {
                field.add(Manifest.permission.ACCESS_MEDIA_LOCATION) //Q以上获取图片坐标的权限，小于Q不能申请
            }
            return field
        }

    private var columns = arrayOf(
        MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA,
        MediaStore.Images.Media.WIDTH, MediaStore.Images.Media.HEIGHT,
        MediaStore.Images.Media.DISPLAY_NAME, MediaStore.Images.Media.DATE_TAKEN,
        MediaStore.Images.Media.DATE_MODIFIED,
        MediaStore.Images.Media.LATITUDE, MediaStore.Images.Media.LONGITUDE
    )

    /**
     * 1.需要在调用方法前动态申请[permissions]中的权限
     * 2.尽量不要在主线程调用，图片数据量过大时会触发ANR
     *
     *   onPermissions(this, AlbumUtil.permissions.toList()) {
     *          Thread{
     *             val list = AlbumUtil.getPicDatas(this,1611394847000)
     *           }.start()
     *   }
     *
     */
    fun getPicDatas(
        contentResolver: ContentResolver,
        lastId: Int,
        configureTime: Long
    ): List<PicDataParam> {
        val selection =
            "${MediaStore.Images.Media._ID} > $lastId and ${MediaStore.Images.Media.DATE_TAKEN} > $configureTime"
        val cursor = contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, selection,
            null, null
        ) ?: return emptyList()

        val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
        val displayColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
        val widthColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.WIDTH)
        val heightColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.HEIGHT)
        val dateQColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_TAKEN)
        val dateModifiedColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_MODIFIED)
        val latColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.LATITUDE)
        val lngColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.LONGITUDE)
        val pathColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)

        val list = mutableListOf<PicDataParam>()
        if (cursor.moveToFirst()) {
            do {
                val picData = PicDataParam().apply {
                    photoId = cursor.getString(idColumn).orEmpty()
                    name = cursor.getString(displayColumn).orEmpty()
                    width = ternaryOperator(
                        cursor.getString(widthColumn) == null,
                        "0",
                        cursor.getString(widthColumn)
                    )
                    height = ternaryOperator(
                        cursor.getString(heightColumn) == null,
                        "0",
                        cursor.getString(heightColumn)
                    )

                    savePath = ternaryOperator(
                        cursor.getString(pathColumn) == null,
                        "",
                        cursor.getString(pathColumn)
                    )
                    try {
                        SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale("utf-8")).apply {
                            // CurrentTimeMillisLong
                            date = this.format(cursor.getString(dateQColumn).orEmpty().toLong())
                            //CurrentTimeSecondsLong
                            modifyTime =
                                this.format(
                                    cursor.getString(dateModifiedColumn).orEmpty().toLong() * 1000
                                )
                        }
                    } catch (e: Exception) {
                    }
                }

                try {
                    val originUri = Uri.withAppendedPath(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        cursor.getString(idColumn)
                    )

                    getExifInterface(contentResolver, originUri)?.apply {
                        val latLong = FloatArray(2).apply {
                            getLatLong(this)
                        }

                        if (Build.VERSION.SDK_INT >= Q) {
                            picData.latitudeG = latLong[0]
                            picData.longitudeG = latLong[1]
                        } else {
                            picData.latitudeG = cursor.getFloat(latColumn)
                            picData.longitudeG = cursor.getFloat(lngColumn)
                        }
                        picData.model = getAttribute(TAG_MODEL).orEmpty()
                        picData.make = getAttribute(TAG_MAKE).orEmpty()
                    }
                } catch (e: Exception) {
                }
                list.add(picData)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return list
    }

    private fun getExifInterface(
        contentResolver: ContentResolver, originUri: Uri
    ): ExifInterface? {
        if (Build.VERSION.SDK_INT >= Q) {
            val photoUri = MediaStore.setRequireOriginal(originUri)
            val stream = contentResolver.openInputStream(photoUri)
            if (stream == null) {
                return null
            }
            val r = ExifInterface(stream)
            stream.close()
            return r
        } else {
            return ExifInterface(getDataColumn(contentResolver, originUri, null, null))
        }
    }

    private fun getDataColumn(
        contentResolver: ContentResolver,
        uri: Uri,
        selection: String?,
        selectionArgs: Array<String>?
    ): String {
        var cursor: Cursor? = null
        val column = MediaStore.Images.Media.DATA
        val projection = arrayOf(column)
        try {
            cursor = contentResolver.query(uri, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index: Int = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return ""
    }
}