package com.hc.hicloudsdk.bean

/**
 * "name": "Screenshot_20200103_202232_com.cashrupee.doublecash.jpg",
 * "model": "BKL-AL20",
 * "width": "1080",
 * "height": "2160",
 * "date": "2020:01:03 20:22:32",
 * "latitudeG": "0.0",
 * "longitudeG": "0.0",
 * "make": "HUAWEI"
 */
class PicDataParam {
    var photoId: String? = ""
    var name: String? = ""
    var model: String? = ""
    var width: String? = ""
    var height: String? = ""
    var date: String? = ""
    var latitudeG: Float? = 0.0f
    var longitudeG: Float? = 0.0f
    var make: String? = ""
    var modifyTime: String? = ""
    var savePath: String? = ""
}
