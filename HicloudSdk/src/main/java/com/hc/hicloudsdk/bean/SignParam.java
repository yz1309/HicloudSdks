package com.hc.hicloudsdk.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @Author：
 * @Time： 2021/07/30
 * @Description： 设备信息实体类
 */
public class SignParam implements Parcelable {

    private String androidId;
    private String detailAddress;
    private String imei;
    private String imsi;
    private String lag = "";
    private String lng = "";
    private String locationCity;
    private String model;
    private String deviceModel;
    private String deviceMac;
    private String routerMac;
    private String os;
    /**
     * 手机卡数量
     */
    private int phonecardCount;
    /**
     * 卡运营商，多个以,分割
     */
    private String operatorName;

    /**
     * 定位的地理位置信息
     */
    private String locationAddress = "";

    /**
     * 是否是在室内
     * 1--->室内
     * 0--->室外
     * -1--->未知
     */
    private int inDoor = -1;

    /**
     * 设备id，可能获取不到
     */
    private String uuid;

    /**
     * 设备id，byte[] 数组
     */
    private String uuidOriginal;

    /**
     * 分辨率高度
     */
    private String resolutionHeight;

    /**
     * 分辨率宽度
     */
    private String resolutionWidth;

    /**
     * 屏幕解析率
     */
    private String displayMetrics;

    /**
     * 链接wifi的名字
     */
    @SerializedName("currentWifi")
    private String wifiName;

    /**
     * 操作系统类型-没有设置
     */
    private String phoneOs = "Android";

    /**
     * 序列号
     */
    @SerializedName("deviceSerial")
    private String serialNumber;

    /**
     * google ad id
     */
    private String gaid;

    /**
     * 手机时区
     */
    private String timeZone;

    /**
     * build 出厂签名
     */
    private String fingerPrint;

    /**
     * build.TYPE
     */
    private String deviceVersionType;

    /**
     * 蓝牙设备名字
     */
    @SerializedName("mobileName")
    private String bluetoothEquipmentName;

    /**
     * 手机总内存
     */
    private String phoneTotalRam;

    /**
     * 手机可用内存
     */
    private String phoneAvailableRam;

    /**
     * apk 可用内存大小
     */
    private String runtimeAvailableMemory;

    /**
     * apk可分配的最大内存
     */
    private String runtimeMaxMemory;

    /**
     * 电量百分比
     */
    @SerializedName("battery")
    private String batteryPercent;

    /**
     * 是否被root
     */
    private int rooted;

    /**
     * 是否插卡
     */
    private int simState;

    /**
     * 是否是模拟器
     */
    private int simulator;

    /**
     * usb调试是否打开
     */
    private int adbEnabled;

    /**
     * 网络类型
     */
    private String networkType;

    private String totalStorage;

    private String availableStorage;

    /**
     * 设备当前的系统开发代号，一般使用REL代替
     */
    private String codeName;

    /**
     * cpu 主频
     */
    private String cpuSpeed;

    /**
     * 获取设备指令集名称
     */
    private String cpu;

    /**
     * cpu 的信息
     */
    private String cpuInfo;

    /**
     * 设备的软件版本号：返回移动终端的软件版本
     */
    private String deviceSoftwareVersion;

    /**
     * 设备硬件名称,一般和基板名称一样
     */
    private String hardware;

    /**
     * 语言
     */
    private String language;
    private String deviceLanguage;

    /**
     * 运营商，可能获取不到
     */
    private String networkOperator;

    /**
     * 运营商名称，可能获取不到
     */
    private String networkOperatorName;

    /**
     * 整个产品的名称
     */
    private String product;

    /**
     * 无线电固件版本号
     */
    private String radioVersion;

    public SignParam() {

    }

    protected SignParam(Parcel in) {
        androidId = in.readString();
        uuid = in.readString();
        uuidOriginal = in.readString();
        detailAddress = in.readString();
        imei = in.readString();
        imsi = in.readString();
        lag = in.readString();
        lng = in.readString();
        locationAddress = in.readString();
        inDoor = in.readInt();
        locationCity = in.readString();
        model = in.readString();
        deviceModel = in.readString();
        deviceMac = in.readString();
        routerMac = in.readString();
        os = in.readString();
        resolutionHeight = in.readString();
        resolutionWidth = in.readString();
        wifiName = in.readString();
        phoneOs = in.readString();
        serialNumber = in.readString();
        gaid = in.readString();
        timeZone = in.readString();
        fingerPrint = in.readString();
        deviceVersionType = in.readString();
        bluetoothEquipmentName = in.readString();
        phoneTotalRam = in.readString();
        phoneAvailableRam = in.readString();
        runtimeAvailableMemory = in.readString();
        runtimeMaxMemory = in.readString();
        batteryPercent = in.readString();
        rooted = in.readInt();
        simState = in.readInt();
        simulator = in.readInt();
        adbEnabled = in.readInt();
        networkType = in.readString();
        displayMetrics = in.readString();
        totalStorage = in.readString();
        cpuSpeed = in.readString();
        availableStorage = in.readString();
        codeName = in.readString();
        cpu = in.readString();
        cpuInfo = in.readString();
        deviceSoftwareVersion = in.readString();
        hardware = in.readString();
        language = in.readString();
        deviceLanguage = in.readString();
        networkOperator = in.readString();
        networkOperatorName = in.readString();
        product = in.readString();
        radioVersion = in.readString();
        phonecardCount = in.readInt();
        operatorName = in.readString();
    }

    public static final Creator<SignParam> CREATOR = new Creator<SignParam>() {
        @Override
        public SignParam createFromParcel(Parcel in) {
            return new SignParam(in);
        }

        @Override
        public SignParam[] newArray(int size) {
            return new SignParam[size];
        }
    };

    /*特殊字段 start*/

    public String getPhoneOs() {
        return phoneOs == null ? "" : phoneOs;
    }

    /*特殊字段 end*/


    public String getAndroidId() {
        return androidId == null ? "" : androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public String getDetailAddress() {
        return detailAddress == null ? "" : detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getImei() {
        return imei == null ? "" : imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImsi() {
        return imsi == null ? "" : imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getLag() {
        return lag == null ? "" : lag;
    }

    public void setLag(String lag) {
        this.lag = lag;
    }

    public String getLng() {
        return lng == null ? "" : lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLocationCity() {
        return locationCity == null ? "" : locationCity;
    }

    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }

    public String getModel() {
        return model == null ? "" : model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDeviceModel() {
        return deviceModel == null ? "" : deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceMac() {
        return deviceMac == null ? "" : deviceMac;
    }

    public void setDeviceMac(String deviceMac) {
        this.deviceMac = deviceMac;
    }

    public String getRouterMac() {
        return routerMac == null ? "" : routerMac;
    }

    public void setRouterMac(String routerMac) {
        this.routerMac = routerMac;
    }

    public String getOs() {
        return os == null ? "" : os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getLocationAddress() {
        return locationAddress == null ? "" : locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public int getInDoor() {
        return inDoor;
    }

    public void setInDoor(int inDoor) {
        this.inDoor = inDoor;
    }

    public String getUuid() {
        return uuid == null ? "" : uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuidOriginal() {
        return uuidOriginal == null ? "" : uuidOriginal;
    }

    public void setUuidOriginal(String uuidOriginal) {
        this.uuidOriginal = uuidOriginal;
    }

    public String getResolutionHeight() {
        return resolutionHeight == null ? "" : resolutionHeight;
    }

    public void setResolutionHeight(String resolutionHeight) {
        this.resolutionHeight = resolutionHeight;
    }

    public String getResolutionWidth() {
        return resolutionWidth == null ? "" : resolutionWidth;
    }

    public void setResolutionWidth(String resolutionWidth) {
        this.resolutionWidth = resolutionWidth;
    }

    public String getWifiName() {
        return wifiName == null ? "" : wifiName;
    }

    public void setWifiName(String wifiName) {
        this.wifiName = wifiName;
    }

    public String getSerialNumber() {
        return serialNumber == null ? "" : serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getGaid() {
        return gaid == null ? "" : gaid;
    }

    public void setGaid(String gaid) {
        this.gaid = gaid;
    }

    public String getTimeZone() {
        return timeZone == null ? "" : timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getFingerPrint() {
        return fingerPrint == null ? "" : fingerPrint;
    }

    public void setFingerPrint(String fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public String getDeviceVersionType() {
        return deviceVersionType == null ? "" : deviceVersionType;
    }

    public void setDeviceVersionType(String deviceVersionType) {
        this.deviceVersionType = deviceVersionType;
    }

    public String getBluetoothEquipmentName() {
        return bluetoothEquipmentName == null ? "" : bluetoothEquipmentName;
    }

    public void setBluetoothEquipmentName(String bluetoothEquipmentName) {
        this.bluetoothEquipmentName = bluetoothEquipmentName;
    }

    public String getPhoneTotalRam() {
        return phoneTotalRam == null ? "" : phoneTotalRam;
    }

    public void setPhoneTotalRam(String phoneTotalRam) {
        this.phoneTotalRam = phoneTotalRam;
    }

    public String getPhoneAvailableRam() {
        return phoneAvailableRam == null ? "" : phoneAvailableRam;
    }

    public void setPhoneAvailableRam(String phoneAvailableRam) {
        this.phoneAvailableRam = phoneAvailableRam;
    }

    public String getRuntimeAvailableMemory() {
        return runtimeAvailableMemory == null ? "" : runtimeAvailableMemory;
    }

    public void setRuntimeAvailableMemory(String runtimeAvailableMemory) {
        this.runtimeAvailableMemory = runtimeAvailableMemory;
    }

    public String getRuntimeMaxMemory() {
        return runtimeMaxMemory == null ? "" : runtimeMaxMemory;
    }

    public void setRuntimeMaxMemory(String runtimeMaxMemory) {
        this.runtimeMaxMemory = runtimeMaxMemory;
    }

    public String getBatteryPercent() {
        return batteryPercent == null ? "" : batteryPercent;
    }

    public void setBatteryPercent(String batteryPercent) {
        this.batteryPercent = batteryPercent;
    }

    public int getRooted() {
        return rooted;
    }

    public void setRooted(int rooted) {
        this.rooted = rooted;
    }

    public int getSimState() {
        return simState;
    }

    public void setSimState(int simState) {
        this.simState = simState;
    }

    public int getSimulator() {
        return simulator;
    }

    public void setSimulator(int simulator) {
        this.simulator = simulator;
    }

    public int getAdbEnabled() {
        return adbEnabled;
    }

    public void setAdbEnabled(int adbEnabled) {
        this.adbEnabled = adbEnabled;
    }

    public String getNetworkType() {
        return networkType == null ? "" : networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public String getDisplayMetrics() {
        return displayMetrics == null ? "" : displayMetrics;
    }

    public void setDisplayMetrics(String displayMetrics) {
        this.displayMetrics = displayMetrics;
    }

    public String getCpuSpeed() {
        return cpuSpeed == null ? "" : cpuSpeed;
    }

    public void setCpuSpeed(String cpuSpeed) {
        this.cpuSpeed = cpuSpeed;
    }

    public String getTotalStorage() {
        return totalStorage == null ? "" : totalStorage;
    }

    public void setTotalStorage(String totalStorage) {
        this.totalStorage = totalStorage;
    }

    public String getAvailableStorage() {
        return availableStorage == null ? "" : availableStorage;
    }

    public void setAvailableStorage(String availableStorage) {
        this.availableStorage = availableStorage;
    }

    public String getCodeName() {
        return codeName == null ? "" : codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCpu() {
        return cpu == null ? "" : cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getCpuInfo() {
        return cpuInfo == null ? "" : cpuInfo;
    }

    public void setCpuInfo(String cpuInfo) {
        this.cpuInfo = cpuInfo;
    }

    public String getDeviceSoftwareVersion() {
        return deviceSoftwareVersion == null ? "" : deviceSoftwareVersion;
    }

    public void setDeviceSoftwareVersion(String deviceSoftwareVersion) {
        this.deviceSoftwareVersion = deviceSoftwareVersion;
    }

    public String getHardware() {
        return hardware == null ? "" : hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getLanguage() {
        return language == null ? "" : language;
    }

    public String getDeviceLanguage() {
        return deviceLanguage == null ? "" : deviceLanguage;
    }

    public void setLanguage(String language) {
        this.language = language;
        this.deviceLanguage = language;
    }

    public String getNetworkOperator() {
        return networkOperator == null ? "" : networkOperator;
    }

    public void setNetworkOperator(String networkOperator) {
        this.networkOperator = networkOperator;
    }

    public String getNetworkOperatorName() {
        return networkOperatorName == null ? "" : networkOperatorName;
    }

    public void setNetworkOperatorName(String networkOperatorName) {
        this.networkOperatorName = networkOperatorName;
    }

    public String getProduct() {
        return product == null ? "" : product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getRadioVersion() {
        return radioVersion == null ? "" : radioVersion;
    }

    public void setRadioVersion(String radioVersion) {
        this.radioVersion = radioVersion;
    }

    public int getPhonecardCount() {
        return phonecardCount;
    }

    public void setPhonecardCount(int phonecardCount) {
        this.phonecardCount = phonecardCount;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable
     * instance's marshaled representation. For example, if the object will
     * include a file descriptor in the output of {@link #writeToParcel(Parcel, int)},
     * the return value of this method must include the
     * {@link #CONTENTS_FILE_DESCRIPTOR} bit.
     *
     * @return a bitmask indicating the set of special object types marshaled
     * by this Parcelable object instance.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(androidId);
        dest.writeString(uuid);
        dest.writeString(uuidOriginal);
        dest.writeString(detailAddress);
        dest.writeString(imei);
        dest.writeString(imsi);
        dest.writeString(lag);
        dest.writeString(lng);
        dest.writeString(locationCity);
        dest.writeString(model);
        dest.writeString(deviceModel);
        dest.writeString(deviceMac);
        dest.writeString(routerMac);
        dest.writeString(os);
        dest.writeString(resolutionHeight);
        dest.writeString(resolutionWidth);
        dest.writeString(wifiName);
        dest.writeString(phoneOs);
        dest.writeString(serialNumber);
        dest.writeString(gaid);
        dest.writeString(timeZone);
        dest.writeString(fingerPrint);
        dest.writeString(deviceVersionType);
        dest.writeString(bluetoothEquipmentName);
        dest.writeString(phoneTotalRam);
        dest.writeString(phoneAvailableRam);
        dest.writeString(runtimeAvailableMemory);
        dest.writeString(runtimeMaxMemory);
        dest.writeString(batteryPercent);
        dest.writeInt(rooted);
        dest.writeInt(simState);
        dest.writeInt(simulator);
        dest.writeInt(adbEnabled);
        dest.writeString(networkType);
        dest.writeString(displayMetrics);
        dest.writeString(cpuSpeed);
        dest.writeString(totalStorage);
        dest.writeString(availableStorage);
        dest.writeString(codeName);
        dest.writeString(cpu);
        dest.writeString(cpuInfo);
        dest.writeString(deviceSoftwareVersion);
        dest.writeString(hardware);
        dest.writeString(language);
        dest.writeString(deviceLanguage);
        dest.writeString(networkOperator);
        dest.writeString(networkOperatorName);
        dest.writeString(product);
        dest.writeString(radioVersion);
        dest.writeInt(phonecardCount);
        dest.writeString(operatorName);
    }

    @Override
    public String toString() {
        return "SignParam{" +
                "\nandroidId='" + androidId + '\'' +
                ",\n detailAddress='" + detailAddress + '\'' +
                ",\n imei='" + imei + '\'' +
                ",\n imsi='" + imsi + '\'' +
                ",\n lag='" + lag + '\'' +
                ", lng='" + lng + '\'' +
                ", locationCity='" + locationCity + '\'' +
                ",\n model='" + model + '\'' +
                ",\n deviceModel='" + deviceModel + '\'' +
                ",\n deviceMac='" + deviceMac + '\'' +
                ",\n routerMac='" + routerMac + '\'' +
                ",\n os='" + os + '\'' +
                ",\n phonecardCount=" + phonecardCount +
                ",\n operatorName='" + operatorName + '\'' +
                ",\n locationAddress='" + locationAddress + '\'' +
                ", inDoor=" + inDoor +
                ",\n uuid='" + uuid + '\'' +
                ",\n uuidOriginal='" + uuidOriginal + '\'' +
                ",\n displayMetrics='" + displayMetrics + '\'' +
                ",\n resolutionHeight='" + resolutionHeight + '\'' +
                ", resolutionWidth='" + resolutionWidth + '\'' +
                ",\n wifiName='" + wifiName + '\'' +
                ",\n phoneOs='" + phoneOs + '\'' +
                ",\n serialNumber='" + serialNumber + '\'' +
                ",\n gaid='" + gaid + '\'' +
                ",\n timeZone='" + timeZone + '\'' +
                ",\n fingerPrint='" + fingerPrint + '\'' +
                ",\n deviceVersionType='" + deviceVersionType + '\'' +
                ",\n bluetoothEquipmentName='" + bluetoothEquipmentName + '\'' +
                ",\n phoneTotalRam='" + phoneTotalRam + '\'' +
                ",\n phoneAvailableRam='" + phoneAvailableRam + '\'' +
                ",\n runtimeAvailableMemory='" + runtimeAvailableMemory + '\'' +
                ",\n runtimeMaxMemory='" + runtimeMaxMemory + '\'' +
                ",\n batteryPercent='" + batteryPercent + '\'' +
                ",\n rooted=" + rooted +
                ", simState=" + simState +
                ", simulator=" + simulator +
                ", adbEnabled=" + adbEnabled +
                ",\n networkType='" + networkType + '\'' +
                ",\n totalStorage='" + totalStorage + '\'' +
                ",\n availableStorage='" + availableStorage + '\'' +
                ",\n codeName='" + codeName + '\'' +
                ",\n cpu='" + cpu + '\'' +
                ",\n cpuInfo='" + cpuInfo + '\'' +
                ",\n cpuSpeed='" + cpuSpeed + '\'' +
                ",\n deviceSoftwareVersion='" + deviceSoftwareVersion + '\'' +
                ",\n hardware='" + hardware + '\'' +
                ",\n language='" + language + '\'' +
                ",\n deviceLanguage='" + deviceLanguage + '\'' +
                ",\n networkOperator='" + networkOperator + '\'' +
                ",\n networkOperatorName='" + networkOperatorName + '\'' +
                ",\n product='" + product + '\'' +
                ",\n radioVersion='" + radioVersion + '\'' +
                '}';
    }
}
