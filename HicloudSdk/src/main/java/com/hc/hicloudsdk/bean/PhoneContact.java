package com.hc.hicloudsdk.bean;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

/**
 * @Author:
 * @Time: 2021/07/29
 * @Description： 联系人实体
 */
public class PhoneContact implements Parcelable, Comparable<PhoneContact> {
    /**
     * 联系人姓名,从手机获取
     */
    private String contactName;
    /**
     * 联系人手机号码
     */
    private String contactPhone;
    /**
     * 与联系人的关系
     */
    private String contactRelation;
    /**
     * 用户手动输入的紧急联系人的姓名
     */
    private String contactRealName;

    public PhoneContact() {}

    public String getContactName() {
        return contactName == null ? "" : contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone == null ? "" : contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactRelation() {
        return contactRelation == null ? "" : contactRelation;
    }

    public void setContactRelation(String contactRelation) {
        this.contactRelation = contactRelation;
    }

    public String getContactRealName() {
        return contactRealName == null ? "" : contactRealName;
    }

    public void setContactRealName(String contactRealName) {
        this.contactRealName = contactRealName;
    }

    @Override
    public int hashCode() {
        return contactPhone.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PhoneContact) {
            PhoneContact phoneContact = (PhoneContact) obj;
            return contactPhone.equals(phoneContact.contactPhone);
        }
        return super.equals(obj);
    }

    @Override
    public int compareTo(@NonNull PhoneContact o) {
        long phoneNumber = Long.parseLong(contactPhone);
        long phoneCompareNumber = Long.parseLong(o.contactPhone);
        return Long.compare(phoneNumber, phoneCompareNumber);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.contactName);
        dest.writeString(this.contactPhone);
        dest.writeString(this.contactRelation);
        dest.writeString(this.contactRealName);
    }

    public void readFromParcel(Parcel source) {
        this.contactName = source.readString();
        this.contactPhone = source.readString();
        this.contactRelation = source.readString();
        this.contactRealName = source.readString();
    }

    protected PhoneContact(Parcel in) {
        this.contactName = in.readString();
        this.contactPhone = in.readString();
        this.contactRelation = in.readString();
        this.contactRealName = in.readString();
    }

    public static final Creator<PhoneContact> CREATOR = new Creator<PhoneContact>() {
        @Override
        public PhoneContact createFromParcel(Parcel source) {
            return new PhoneContact(source);
        }

        @Override
        public PhoneContact[] newArray(int size) {
            return new PhoneContact[size];
        }
    };
}
