package com.hc.hicloudsdk.bean

/**
 * @param id 短信id
 * @param phoneNumber 号码
 * @param smsType 类型[发送、接收等]
 * @param smsContent 短信内容
 * @param smsTime 短信时间
 */
data class SmsDataBean(
    var smsId: Int,
    var phoneNumber: String? = "",
    var smsType: String? = "",
    var smsContent: String? = "",
    var smsTime: String? = ""
)