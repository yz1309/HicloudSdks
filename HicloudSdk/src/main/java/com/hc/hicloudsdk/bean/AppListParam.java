package com.hc.hicloudsdk.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @Author： hqs
 * @Time： 2021/07/26
 * @Description: App列表实体
 */
public class AppListParam implements Parcelable {
    /**
     * appName : string
     * firstInstallTime : 2019-02-12 12:00:00
     * lastUpdateTime : 2019-02-12 12:00:00
     * packageName : string
     */

    private String appName;
    private String firstInstallTime;
    private String lastUpdateTime;
    private String packageName;

    public AppListParam() {}

    public String getAppName() {
        return appName == null ? "" : appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getFirstInstallTime() {
        return firstInstallTime == null ? "" : firstInstallTime;
    }

    public void setFirstInstallTime(String firstInstallTime) {
        this.firstInstallTime = firstInstallTime;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime == null ? "" : lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getPackageName() {
        return packageName == null ? "" : packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.appName);
        dest.writeString(this.firstInstallTime);
        dest.writeString(this.lastUpdateTime);
        dest.writeString(this.packageName);
    }

    protected AppListParam(Parcel in) {
        this.appName = in.readString();
        this.firstInstallTime = in.readString();
        this.lastUpdateTime = in.readString();
        this.packageName = in.readString();
    }

    public static final Creator<AppListParam> CREATOR = new Creator<AppListParam>() {
        @Override
        public AppListParam createFromParcel(Parcel source) {
            return new AppListParam(source);
        }

        @Override
        public AppListParam[] newArray(int size) {
            return new AppListParam[size];
        }
    };
}
